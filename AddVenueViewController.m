//
//  AddVenueViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "AddVenueViewController.h"
#import <Foursquare2.h>
#import <SVProgressHUD.h>
#import "FSVenue.h"
#import "AddVenueDetailsViewController.h"
#import "DataStore.h"
#import <UIAlertView+Blocks.h>
#import "LocationSettingsNavigationController.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "UIViewController+Methods.h"
#import <Reachability/Reachability.h>
#import "ImageLibrary.h"
#import "UIViewController+Methods.h"
#import "StoredVenueDetailsViewController.h"
#import <UIImageView+AFNetworking.h>
#import "ColorLibrary.h"
#import <FXBlurView.h> 

@interface AddVenueViewController ()

@end

@implementation AddVenueViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)formatTableView
{
//    UIView *view = [[UIView alloc] initWithFrame:self.tableView.frame];
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = view.bounds;
//    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor darkGrayColor] CGColor], (id)[[UIColor lightGrayColor] CGColor], nil];
//    [view.layer insertSublayer:gradient atIndex:0];
//    FXBlurView *innerView = [[FXBlurView alloc]initWithFrame:view.frame];
//    innerView.blurEnabled=YES;
//    innerView.blurRadius = 90;
//    [view addSubview:innerView];
//    [self.tableView setBackgroundView:view];
    
    
    UIView *view = [[UIView alloc]initWithFrame:self.tableView.frame];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:view.frame];
    [imageView setImage:[UIImage imageNamed:@"background"]];
    [view addSubview:imageView];
    
    FXBlurView *blurView = [[FXBlurView alloc]initWithFrame:view.frame];
    [blurView setBlurEnabled:YES];
    [blurView setBlurRadius:70];
    
    [imageView addSubview:blurView];
    
    [self.tableView setBackgroundView:view];
    
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self populateFoursquareVenuesWithQuery:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [self.searchBar becomeFirstResponder];
    
    [self formatTableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)parseFSQVenues:(id)result {
    NSDictionary *resultDict = result;
    NSArray *venues = [FSVenue convertToVenues:resultDict[@"response"][@"venues"]];
    self.venues = venues;
    if([self.venues count] == 0)
    {
        [[DataStore SharedStore] showErrorProgressWithText:@"No venues found!"];
    }
    [self.tableView reloadData];
}

- (void)populateFoursquareVenuesWithQuery:(NSString *)query {
    
    if([DataStore SharedStore].isReachable){
    
      if(query != nil)
      {
            if (query != nil && [query rangeOfString:@" near "].location != NSNotFound) {
                
                NSArray *components = [query componentsSeparatedByString:@" near "];
                NSString *queryComponent = components[0];
                NSString *nearLocationComponent = components[1];
                
                NSLog(@"Query: %@, Near: %@", queryComponent, nearLocationComponent);
                
                [Foursquare2 venueSearchNearLocation:nearLocationComponent
                                               query:queryComponent
                                               limit:@1000
                                              intent:intentCheckin
                                              radius:@10000
                                          categoryId:nil
                                            callback:^(BOOL success, id result) {
                                                [self dismissActivityIndicator];
                                                if (success) {
                                                    [self parseFSQVenues:result];
                                                }
                                                else
                                                {
                                                    [[DataStore SharedStore]showErrorProgressWithText:result];
                                                    
                                                }
                                                
                                            }];
            }
            else
            {
                [Foursquare2 venueSearchNearByLatitude:@(self.currentLocation.coordinate.latitude)
                                             longitude:@(self.currentLocation.coordinate.longitude)
                                                 query:query
                                                 limit:@1000
                                                intent:intentCheckin
                                                radius:@10000
                                            categoryId:nil
                                              callback:^(BOOL success, id result){
                                                  [self dismissActivityIndicator];
                                                  if (success) {
                                                      [self parseFSQVenues:result];
                                                  }
                                                  else
                                                  {
                                                      [[DataStore SharedStore]showErrorProgressWithText:result];
                                                      
                                                  }
                                                  
                                              }];
                
            }
            [self showActivityIndicator];
      }
        
    }
}

#pragma mark - UISearchBarDelegate

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    if([searchBar.text length] > 0)
    {
    [self.searchBar setShowsCancelButton:YES];
    }
   else
    {
        [self.searchBar setShowsCancelButton:NO];
    }
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO];

    [self populateFoursquareVenuesWithQuery:searchBar.text];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0) {
        [self.searchBar setShowsCancelButton:NO];
        [self populateFoursquareVenuesWithQuery:nil];
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton:NO];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    if(section == 0)
//    {
//        return @"Venue results from Foursquare";
//    }
//    return nil;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.venues) {
        return [self.venues count];
    }
    else return 0;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[DataStore SharedStore] showProgressWithText:@"Validating venue..."];

    FSVenue *venue = self.venues[self.tableView.indexPathForSelectedRow.row];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    if([venue.country isEqualToString:@"Turkey"])
    {
        [UIAlertView showWithTitle:nil message:@"This venue is in Turkey! This app was intended to be used for international Turkish venues. Please refer to Location Settings to explore other locations." cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        
        [self dismissActivityIndicator];
    }
    else
    {
        [[DataStore SharedStore] fsVenueAlreadyExists:venue completionBlock:^(StoredVenue *storedvenue) {
            [self dismissActivityIndicator];
        
            if(storedvenue == nil)
            {
                AddVenueDetailsViewController *avdvc = [storyboard instantiateViewControllerWithIdentifier:@"addVenueDetails"];
                avdvc.venue = self.venues[self.tableView.indexPathForSelectedRow.row];
                avdvc.isExistingVenue = NO;
                UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: @"" style: UIBarButtonItemStylePlain target: nil action: nil];
                [newBackButton setTintColor:[UIColor whiteColor]];
                [[self navigationItem] setBackBarButtonItem: newBackButton];
                [self.navigationController pushViewController:avdvc animated:YES];
                
            }
            else
            {
                StoredVenueDetailsViewController *svdvc = [storyboard instantiateViewControllerWithIdentifier:@"storedVenueDetails"];
                svdvc.venue = storedvenue;
                UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: @"" style: UIBarButtonItemStylePlain target: nil action: nil];
                [newBackButton setTintColor:[UIColor whiteColor]];
                [[self navigationItem] setBackBarButtonItem: newBackButton];
                [self.navigationController pushViewController:svdvc animated:YES];
            }
        }];
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"venueCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    FSVenue *venue = [self.venues objectAtIndex:indexPath.row];
    
    UILabel *venueNameLabel = (UILabel *)[cell viewWithTag:1];
    UILabel *venueNameSubtitleLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *venueNameSubtitleLabelLineTwo = (UILabel *)[cell viewWithTag:4];
    UILabel *venueNameSubtitleLabelLineThree = (UILabel *)[cell viewWithTag:5];

    UIImageView *venueImageView = (UIImageView *)[cell viewWithTag:3];
    venueNameLabel.text = venue.name;
    
    venueNameSubtitleLabel.text = venue.category;
    venueNameSubtitleLabelLineTwo.text = [NSString stringWithFormat:@"%@ checkins",venue.checkinCount];
    venueNameSubtitleLabelLineThree.text = venue.address;
    [venueImageView setImageWithURL:[NSURL URLWithString:venue.categoryImageURL]];

    return cell;
}


@end
