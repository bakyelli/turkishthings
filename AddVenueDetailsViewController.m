//
//  AddVenueDetailsViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "AddVenueDetailsViewController.h"
#import "NewVenueDetailsTableViewController.h"
#import "DataStore.h"
@interface AddVenueDetailsViewController ()

@property (nonatomic) NewVenueDetailsTableViewController *nvdtc;
@end

@implementation AddVenueDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"NewVenueTableView"])
    {
        self.nvdtc = segue.destinationViewController;
        self.nvdtc.isExistingVenue = self.isExistingVenue;
        
        if(self.isExistingVenue)
        {
            self.nvdtc.existingVenue = self.existingVenue;
        }
        else
        {
          self.nvdtc.venue = self.venue;
        }
        
    }
}

- (IBAction)addButtonPressed:(id)sender {
    [self.nvdtc btnSubmitPressed];

}
@end
