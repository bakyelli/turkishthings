//
//  FSVenue.m
//  APIsAndBlocks
//
//  Created by Basar Akyelli on 10/24/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "FSVenue.h"

@implementation FSVenue


-(id)initWithName:(NSString *)name
          venueID:(NSString *)venueID
         latitude:(NSNumber *)lat
       longtitude:(NSNumber *)lgt
         distance:(NSNumber *)distance
         category:(NSString *)category
             city:(NSString *)city
          country:(NSString *)country
     foursquareID:(NSString *)foursquareID
          address:(NSString *)address
          zipCode:(NSString *)zipCode
     checkinCount:(NSNumber *)checkinCount
         tipCount:(NSNumber *)tipCount
           webURL:(NSString *)webURL
 categoryImageURL:(NSString *)categoryImageURL;
{
    self = [super init];
    
    if(self)
    {
        _name = name;
        _venueID = venueID;
        _latitude = lat;
        _longtitude = lgt;
        _distance = distance;
        _category = category;
        _city = city;
        _country = country;
        _foursquareID = foursquareID;
        _address = address;
        _zipCode = zipCode;
        _checkinCount = checkinCount;
        _tipCount = tipCount;
        _webURL = webURL;
        _categoryImageURL = categoryImageURL;
        
        if(_name == nil)
            _name = @"";
        
        if(_venueID == nil)
            _venueID = @"";
        
        if(_latitude == nil)
            _latitude = @0;
        
        if(_longtitude == nil)
            _longtitude = @0;
        
        if(_distance == nil)
            _distance = @0;
        
        if(_category == nil)
            _category = @"";
        
        if(_city == nil)
            _city = @"";
        
        if(_country == nil)
            _country = @"";
        
        if(_foursquareID == nil)
            _foursquareID = @"";
        
        if(_address == nil)
            _address = @"";
        
        if(_zipCode == nil)
            _zipCode = @"";
        
        if(_checkinCount == nil)
            _checkinCount = @0;
        
        if(_tipCount == nil)
            _tipCount = @0;
        
        if(_webURL == nil)
            _webURL = @"";
        
        if(_categoryImageURL == nil)
            _categoryImageURL = @"";
            
            
    }
    return self;
}

-(id)initWithVenueDictionary:(NSDictionary *)venue
{
    NSString *venuecategory = @"";
    NSString *venuecategoryImageURL = @"";
    
    if([venue[@"categories"] count] > 0)
    {
        venuecategory = venue[@"categories"][0][@"name"];
        venuecategoryImageURL = [NSString stringWithFormat:@"%@88%@", venue[@"categories"][0][@"icon"][@"prefix"], venue[@"categories"][0][@"icon"][@"suffix"]];
        
    }
    else
    {
        venuecategoryImageURL = @"https://foursquare.com/img/categories_v2/building/home_88.png";
    }
    
    return [self initWithName:venue[@"name"]
                      venueID:venue[@"id"]
                     latitude:[NSNumber numberWithDouble:[venue[@"location"][@"lat"] doubleValue]]
                   longtitude:[NSNumber numberWithDouble:[venue[@"location"][@"lng"] doubleValue]]
                     distance:[NSNumber numberWithDouble:[venue[@"location"][@"distance"] doubleValue]]
                     category:venuecategory
                         city:venue[@"location"][@"city"]
                      country:venue[@"location"][@"country"]
                 foursquareID:venue[@"id"]
                      address:venue[@"location"][@"address"]
                      zipCode:venue[@"location"][@"postalCode"]
            checkinCount:[NSNumber numberWithDouble:[venue[@"stats"][@"checkinsCount"] doubleValue]]
                     tipCount:[NSNumber numberWithDouble:[venue[@"stats"][@"tipCount"] doubleValue]]
                       webURL:venue[@"url"]
             categoryImageURL:venuecategoryImageURL];
}

+(NSArray *)convertToVenues:(NSArray *)venues
{
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:[venues count]];
    
    for(NSDictionary *venue in venues)
    {
        FSVenue *myVenue = [[FSVenue alloc]initWithVenueDictionary:venue];
        
        [objects addObject:myVenue];
        
    }
    return objects;
}




@end
