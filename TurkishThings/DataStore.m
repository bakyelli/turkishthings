//
//  DataStore.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "DataStore.h"
#import <Parse/Parse.h>
#import "VenueCategory.h"
#import <UIAlertView+Blocks.h>


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@implementation DataStore




+ (DataStore *)SharedStore {
    static DataStore *sharedDataStore = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDataStore = [[self alloc] init];
    });
    return sharedDataStore;
}


-(id) init{
    self = [super init];
    if (self)
    {
    }
    return self;
}
-(UIColor *)UIColorFromRGBMethod:(unsigned long int)rgbValue
{
    return UIColorFromRGB(rgbValue);
}

-(void)getAvailableCategoriesWithCompletion:(void (^)(void))completion
{
    NSLog(@"Populating categories");
 //   NSLog(@"There are %d categories",[self.venueCategories count]);
    [self.venueCategories removeAllObjects];
    self.venueCategories = [[NSMutableArray alloc]init];
    
    PFQuery *query = [PFQuery queryWithClassName:@"VenueCategory"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error)
        {
            [self.venueCategories removeAllObjects];
            for(PFObject *object in objects)
            {
                VenueCategory *category = [[VenueCategory alloc]initWithName:object[@"categoryName"]];
                [self.venueCategories addObject:category];
            }
            completion();

        }
    }];

}

-(void)launchFoursquareWithVenueID:(NSString *)venueID
{
    NSString *foursquareAppLink = [NSString stringWithFormat:@"foursquare://venues/%@?ref=%@",venueID, @"1XRBSMKNJMFYPZZPWCAIKY2I0PMHPBZ1BW0BTGTJ1TBY02ZY"];
    NSString *foursquareWebLink = [NSString stringWithFormat:@"http://foursquare.com/venue/%@",venueID];
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"foursquare://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:foursquareAppLink]];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:foursquareWebLink]];
        
    }
    
}
-(void)getStoredVenuesForACity:(NSString *)city withCompletion:(void (^) (NSArray *))block
{
    PFQuery *query = [PFQuery queryWithClassName:@"StoredVenue"];
    [query whereKey:@"city" equalTo:city];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        for(PFObject *object in objects)
        {
            NSLog(@"Venue name: %@", object[@"name"]);
        }
        
    }];
    
}
-(void)getStoredVenuesWithVenueFilter:(VenueFilter *)filter withCompletion:(void (^) (NSMutableArray *))block
{
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"StoredVenue"];
    
    if(filter!=nil)
    {
        if(filter.filterWithVenueCategory)
        {
            [query whereKey:@"category" equalTo:filter.venueCategory.categoryName];
        }
        if(filter.filterWithLocation)
        {
            PFGeoPoint *locationPoint = [PFGeoPoint geoPointWithLocation:filter.location];
            [query whereKey:@"location" nearGeoPoint:locationPoint withinMiles:100];

        }
//        if(filter.filterWithSearchQuery)
//        {
//            TO DO!
//        }
    }
    
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            NSMutableArray *storedVenuesArray = [[NSMutableArray alloc]init];
        for(PFObject *object in objects)
        {
            StoredVenue *storedVenue = [[StoredVenue alloc]initWithParseObject:object];
            [storedVenuesArray addObject:storedVenue];
        }
            block(storedVenuesArray);
    }];
    
}
-(void)getStoredVenueReviewsForStoredVenueID:(NSString *)storedVenueUniqueID withCompletion:(void (^) (NSMutableArray *))block
{
    PFQuery *query = [PFQuery queryWithClassName:@"StoredVenueReview"];
    [query whereKey:@"storedVenueUniqueID" equalTo:storedVenueUniqueID];
    [query addDescendingOrder:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *storedVenueReviewsArray = [[NSMutableArray alloc]init];
        for(PFObject *object in objects)
        {
            StoredVenueReview *storedVenue = [[StoredVenueReview alloc]initWithParseObject:object];
            [storedVenueReviewsArray addObject:storedVenue];
        }
        block(storedVenueReviewsArray);

    }];
}
-(void)getAllStoredVenueImageThumbnailsForStoredVenueID:(NSString *)storedVenueUniqueID withCompletion:(void (^) (NSMutableArray *))block
{
    PFQuery *query = [PFQuery queryWithClassName:@"StoredVenueImage"];
    [query selectKeys:@[@"createdByUserName", @"createdByUserReviewerName", @"uniqueID",@"storedVenueUniqueID",@"reviewImageThumbnail"]];
    
    [query whereKey:@"storedVenueUniqueID" equalTo:storedVenueUniqueID];
    [query addDescendingOrder:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *storedVenueImagesArray= [[NSMutableArray alloc]init];
        for(PFObject *object in objects)
        {
            StoredVenueImage *storedVenueImage = [[StoredVenueImage alloc]initWithParseObjectForThumbnails:object];
            [storedVenueImagesArray addObject:storedVenueImage];
        }
        block(storedVenueImagesArray);
    }];
}

-(void)getVenueFullSizedImageWithUniqueID:(NSString *)venueImageUniqueID withCompletion:(void (^) (StoredVenueImage *))block
{
    PFQuery *query = [PFQuery queryWithClassName:@"StoredVenueImage"];
    [query selectKeys:@[@"reviewImage"]];
    
    [query whereKey:@"uniqueID" equalTo:venueImageUniqueID];
    
    
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        block([[StoredVenueImage alloc]initWithParseObjectForSingleFullSizedImage:object]);
    }];
}

-(void)saveStoredVenueToCloud:(StoredVenue *)venue completionBlock:(void (^)(BOOL))block
{
    PFObject *object = [PFObject objectWithClassName:@"StoredVenue"];
    object[@"name"] = venue.title;
    object[@"category"] = venue.category;
    object[@"city"] = venue.city;
    object[@"country"] = venue.country;
    object[@"ratingCount"] = venue.ratingCount;
    object[@"ratingTotal"] = venue.ratingTotal;
    object[@"location"] = [PFGeoPoint geoPointWithLatitude:venue.coordinate.latitude longitude:venue.coordinate.longitude];
    object[@"longtitude"] = [NSNumber numberWithDouble:venue.coordinate.longitude];
    object[@"latitude"] = [NSNumber numberWithDouble:venue.coordinate.latitude];
    object[@"createdByUserName"] = venue.createdByUserName;
    object[@"createdByUserReviewerName"] = venue.createdByUserReviewerName;
    object[@"foursquareVenueID"] = venue.foursquareVenueID;
    object[@"uniqueID"] =venue.uniqueID;
    object[@"address"] = venue.address;
    object[@"zipCode"] = venue.zipCode;
    object[@"webURL"] = venue.webURL;
    object[@"user"] = [PFUser currentUser];

    
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        block(succeeded);
    }];

}

-(void)saveStoredVenueReviewToCloud:(StoredVenueReview *)venueReview completionBlock:(void (^)(BOOL))block
{
    PFObject *object = [PFObject objectWithClassName:@"StoredVenueReview"];
   
    object[@"createdByUserName"] = venueReview.createdByUserName;
    object[@"createdByUserReviewerName"] = venueReview.createdByUserReviewerName;
    object[@"review"] = venueReview.review;
    object[@"rating"] = venueReview.rating;
    object[@"uniqueID"] =venueReview.uniqueID;
    object[@"storedVenueUniqueID"] = venueReview.storedVenueUniqueID;
    object[@"user"] = [PFUser currentUser];
    
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        block(succeeded);
    }];

}
-(void)saveStoredVenueImageToCloud:(StoredVenueImage *)venueImage completionBlock:(void (^)(BOOL))block
{
    PFObject *object = [PFObject objectWithClassName:@"StoredVenueImage"];
    
    
    object[@"createdByUserName"] = venueImage.createdByUserName;
    object[@"createdByUserReviewerName"] = venueImage.createdByUserReviewerName;
    object[@"uniqueID"] =venueImage.uniqueID;
    object[@"storedVenueUniqueID"] = venueImage.storedVenueUniqueID;
    
    if(venueImage.reviewImage != nil){
        object[@"reviewImage"] = venueImage.reviewImage;
    }
    if(venueImage.reviewImageThumbnail != nil)
    {
        object[@"reviewImageThumbnail"] = venueImage.reviewImageThumbnail;
    }
    
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        block(succeeded);
    }];
    
}
-(void)reportVenueWithReport:(ReportVenue *)reportVenue
{
    PFObject *object = [PFObject objectWithClassName:@"StoredVenueReport"];
    
    object[@"createdByUserName"] = reportVenue.createdByUserName;
    object[@"createdByUserReviewerName"] = reportVenue.createdByUserReviewerName;
    object[@"reportVenueReasonType"] = reportVenue.reportVenueReasonType;
    object[@"uniqueID"] =reportVenue.uniqueID;
    object[@"storedVenueUniqueID"] = reportVenue.storedVenueUniqueID;
    object[@"storedVenueName"] = reportVenue.venueName;
    object[@"user"] = [PFUser currentUser];
    [object saveInBackground];
}

-(void) deleteStoredVenueWithParseObject:(PFObject *)storedVenue
{
    [storedVenue deleteEventually];
    
}
-(void) deleteStoredVenueReviewWithParseObject:(PFObject *)storedVenueReview
{
    [storedVenueReview deleteEventually];
}

- (void)recordUserLastLogin
{
    PFUser *user = [PFUser currentUser];
    user[@"lastLoggedIn"] = [NSDate date];
    [user saveEventually];
}

-(void)showSuccessProgressWithText:(NSString *)text
{
    [[SVProgressHUD appearance] setHudBackgroundColor:UIColorFromRGB(0xCC0A24)];
    [[SVProgressHUD appearance] setHudForegroundColor:[UIColor whiteColor]];
    [[SVProgressHUD appearance] setHudFont:[UIFont fontWithName:@"Avenir" size:16]];
    [[SVProgressHUD appearance] setHudStatusShadowColor:[UIColor greenColor]];
    [SVProgressHUD showSuccessWithStatus:text];
}
-(void)showProgressWithText:(NSString *)text
{
    [[SVProgressHUD appearance] setHudBackgroundColor:UIColorFromRGB(0xCC0A24)];
    [[SVProgressHUD appearance] setHudForegroundColor:[UIColor whiteColor]];
    [[SVProgressHUD appearance] setHudFont:[UIFont fontWithName:@"Avenir" size:16]];
    [[SVProgressHUD appearance] setHudStatusShadowColor:[UIColor greenColor]];
    [SVProgressHUD showWithStatus:text];
}
-(void)showErrorProgressWithText:(NSString *)text
{
    [[SVProgressHUD appearance] setHudBackgroundColor:UIColorFromRGB(0xCC0A24)];
    [[SVProgressHUD appearance] setHudForegroundColor:[UIColor whiteColor]];
    [[SVProgressHUD appearance] setHudFont:[UIFont fontWithName:@"Avenir" size:16]];
    [[SVProgressHUD appearance] setHudStatusShadowColor:[UIColor greenColor]];
    [SVProgressHUD showErrorWithStatus:text];
}


- (void)setNavBarStyle
{
    [[UINavigationBar appearance] setBarTintColor:[[DataStore SharedStore]UIColorFromRGBMethod:0xCC0A24]];
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           shadow, NSShadowAttributeName,
                                                           [UIFont fontWithName:@"HelveticaNeue-Light" size:21.0], NSFontAttributeName, nil]];
    
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void) fsVenueAlreadyExists:(FSVenue *)venue completionBlock:(void (^)(StoredVenue *))block
{
    PFQuery *query = [PFQuery queryWithClassName:@"StoredVenue"];
    [query whereKey:@"foursquareVenueID" equalTo:venue.foursquareID];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error)
        {
            if([objects count] == 0)
            {
                block(nil);
            }
            else{
                PFObject *venueObject = objects[0];
                StoredVenue *existingVenue = [[StoredVenue alloc]initWithParseObject:venueObject];
                block(existingVenue);
            }
        }
        else
        {
            NSLog(@"Error: %@", error);
        }
        
    }];
}


@end
