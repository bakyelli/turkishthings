//
//  StoredVenue.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/7/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "StoredVenue.h"
#import "StoredVenueReview.h"
#import "VenueCategory.h"

@implementation StoredVenue

-(id) initWithParseObject:(PFObject *)object
{
    self = [super init];
    if (self)
    {
        self.coordinate = CLLocationCoordinate2DMake([object[@"latitude"] doubleValue], [object[@"longtitude"] doubleValue]);
        self.title = object[@"name"];
        self.category = object[@"category"];
        self.city = object[@"city"];
        self.country = object[@"country"];
        self.createdByUserName = object[@"createdByUserName"];
        self.createdByUserReviewerName = object[@"createdByUserReviewerName"][0];
        self.foursquareVenueID = object[@"foursquareVenueID"];
        self.ratingTotal = [NSNumber numberWithInt:[object[@"ratingTotal"] integerValue]];
        self.ratingCount = [NSNumber numberWithInt:[object[@"ratingCount"] integerValue]];
        self.address = object[@"address"];
        self.zipCode = object[@"zipCode"];
        self.uniqueID = object[@"uniqueID"];
        self.webURL = object[@"webURL"];
        self.parseObject = object;
        
        //    [self numberOfReviews];
        
    }
    return self;
    
}

-(void)setNumberOfReviewsForStoredVenue
{
    PFQuery *query = [PFQuery queryWithClassName:@"StoredVenueReview"];
    [query whereKey:@"storedVenueUniqueID" equalTo:self.uniqueID];
    self.numberOfReviews = [NSNumber numberWithInteger:[query countObjects]];
}

-(id) init{
    
    self = [super init];
    
    if(self){
        self.uniqueID = [[NSProcessInfo processInfo] globallyUniqueString];
        self.ratingCount = @0;
    }
    
    return self;
}

- (MKAnnotationView *)annotationView
{
    MKAnnotationView *annotationView = [[MKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"StoredVenueAnnotation"];
    
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    annotationView.image = [UIImage imageNamed:@"newLocation.png"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[VenueCategory categoryIconForCategoryString:self.category withSize:15 withColor:[UIColor blackColor]]];
    [imageView setFrame:CGRectMake(0, 0, 20, 20)];
    [annotationView setLeftCalloutAccessoryView:imageView];


    return annotationView;
    
}
- (int) calculateRating
{
    if([self.ratingCount integerValue] > 0 && [self.ratingCount integerValue] > 0)
    {
        return [self.ratingTotal integerValue] / [self.ratingCount integerValue];
    }
    else{
        return 0;
    }
}

- (int) calculateRatingAndUpdateWithReview:(StoredVenueReview *)review
{
    
    PFQuery *query = [PFQuery queryWithClassName:@"StoredVenue"];
    PFObject *object = [query getObjectWithId:self.parseObject.objectId];
    
    NSNumber *currentRatingTotal = [NSNumber numberWithInt:[object[@"ratingTotal"] integerValue]];
    NSNumber *currentRatingCount  = [NSNumber numberWithInt:[object[@"ratingCount"] integerValue]];
    
    self.ratingCount =[NSNumber numberWithInt:[currentRatingCount integerValue] + 1];
    self.ratingTotal =[NSNumber numberWithInt:[currentRatingTotal integerValue] + [review.rating integerValue]];
    
    object[@"ratingTotal"] =self.ratingTotal;
    object[@"ratingCount"] = self.ratingCount;
    [object saveEventually];
    return [self calculateRating];
    
}
- (void) incrementReviewCount
{
    PFObject *object = self.parseObject;
    [object incrementKey:@"reviewCount"];
    [object saveInBackground];
}

@end
