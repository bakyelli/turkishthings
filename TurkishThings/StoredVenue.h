//
//  StoredVenue.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/7/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "FSVenue.h"
#import <Parse/Parse.h>
#import <MapKit/MapKit.h>
#import "StoredVenueReview.h"

@interface StoredVenue : NSObject <MKAnnotation>

@property (copy, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *country;
@property (nonatomic) BOOL isFromFoursquare;
@property (strong, nonatomic) NSString *foursquareVenueID;
@property (strong, nonatomic) FSVenue *foursquareVenue;
@property (strong, nonatomic) NSNumber *ratingCount;
@property (strong, nonatomic) NSNumber *ratingTotal;
@property (strong, nonatomic) NSString *createdByUserName;
@property (strong, nonatomic) NSString *createdByUserReviewerName;
@property (strong, nonatomic) NSString *uniqueID;
@property (strong ,nonatomic) NSString *address;
@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) NSString *webURL;

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (strong,nonatomic) PFObject *parseObject;
@property (strong,nonatomic) NSNumber *numberOfReviews;


-(id) initWithParseObject:(PFObject *)object;
-(MKAnnotationView *)annotationView;
- (int) calculateRating; 
- (int) calculateRatingAndUpdateWithReview:(StoredVenueReview *)review;
- (void) incrementReviewCount;

@end
