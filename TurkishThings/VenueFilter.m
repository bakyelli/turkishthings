//
//  VenueFilter.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/22/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "VenueFilter.h"

@implementation VenueFilter

-(id)init{
    self = [super init];
    if(self)
    {
        self.filterWithLocation = NO;
        self.filterWithSearchQuery = NO;
        self.filterWithVenueCategory = NO;
        
        
    }
    return self;
}
@end
