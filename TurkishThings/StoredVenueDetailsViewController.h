//
//  StoredVenueDetailsViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/8/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoredVenue.h"
#import <EDStarRating.h>

@interface StoredVenueDetailsViewController : UIViewController<EDStarRatingProtocol,UITextFieldDelegate, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>


@property (strong,nonatomic) StoredVenue *venue;
@property (weak, nonatomic) IBOutlet UILabel *lblVenueName;
@property (weak, nonatomic) IBOutlet EDStarRating *starRatingImage;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressInfo;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *reviews;

@property (weak, nonatomic) IBOutlet UIButton *mapsButton;

@end
