//
//  ReportVenue.h
//  TurkishThings
//
//  Created by Basar Akyelli on 12/12/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "StoredVenue.h"

@interface ReportVenue : NSObject

@property (strong,nonatomic) NSString *uniqueID;
@property (strong,nonatomic) NSString *storedVenueUniqueID;
@property (strong,nonatomic) NSString *createdByUserName;
@property (strong,nonatomic) NSString *createdByUserReviewerName;
@property (strong,nonatomic) NSString *venueName;
@property (strong,nonatomic) NSString *reportVenueReasonType;


-(id) initWithParseObject:(PFObject *)object;


@end
