//
//  StoredVenueImages.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/30/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface StoredVenueImage : NSObject

@property (strong,nonatomic) NSString *uniqueID;
@property (strong,nonatomic) NSString *storedVenueUniqueID;
@property (strong,nonatomic) NSString *createdByUserName;
@property (strong,nonatomic) NSString *createdByUserReviewerName;
@property (strong,nonatomic) PFFile *reviewImage;
@property (strong,nonatomic) PFFile *reviewImageThumbnail;

-(id) initWithParseObjectForThumbnails:(PFObject *)object;
-(id) initWithParseObjectForSingleFullSizedImage:(PFObject *)object;



@end
