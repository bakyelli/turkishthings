//
//  WebViewViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 12/2/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "WebViewViewController.h"

@interface WebViewViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation WebViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.containerView addSubview:self.venuePreLoadedWebView];
    if(self.storedVenue){
    [self setTitle:self.storedVenue.title];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
