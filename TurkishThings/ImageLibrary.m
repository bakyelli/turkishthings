//
//  ImageLibrary.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/28/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "ImageLibrary.h"
#import <FAKIonIcons.h>
#import <Parse/Parse.h>
#import <FontAwesomeKit.h>


@implementation ImageLibrary

+(UIImage *) ProfilePicturePlaceholderImageWithSize:(float)size
{
    FAKIonIcons *profilePicPlaceHolderIcon = [FAKIonIcons ios7PersonIconWithSize:size];
    return [profilePicPlaceHolderIcon imageWithSize:CGSizeMake(size, size)];
}
+(UIImage *) ProfilePictureImageWithPlaceHolderSize:(float)size
{
    NSData *profilePictureData =[PFUser currentUser][@"profilePicture"];
    if(profilePictureData == nil)
    {
        return [self ProfilePicturePlaceholderImageWithSize:100];
    }
    else
    {
    return [UIImage imageWithData:profilePictureData];
    }
}
+(UIImage *) MenuSettingsLogOffIconImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *settingsIcon = [FAKIonIcons powerIconWithSize:size];
    [settingsIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [settingsIcon imageWithSize:CGSizeMake(size, size)];
}
+(UIImage *) MenuSettingsLocationIconImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKFontAwesome *locationIcon = [FAKFontAwesome globeIconWithSize:size];
    [locationIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [locationIcon imageWithSize:CGSizeMake(size, size)];
}
+(UIImage *) MapsImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *mapsIcon = [FAKIonIcons mapIconWithSize:size];
    [mapsIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [mapsIcon imageWithSize:CGSizeMake(size,size)];
}
+(UIImage *) ImagesImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *imagesIcon = [FAKIonIcons imagesIconWithSize:size];
    [imagesIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [imagesIcon imageWithSize:CGSizeMake(size,size)];
}
+(UIImage *) HyperlinkImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *linkIcon = [FAKIonIcons linkIconWithSize:size];
    [linkIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [linkIcon imageWithSize:CGSizeMake(size,size)];
}
+(UIImage *) DismissImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *dismissIcon = [FAKIonIcons closeCircledIconWithSize:size];
    [dismissIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [dismissIcon imageWithSize:CGSizeMake(size,size)];
}
+(UIImage *) StarImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *starIcon = [FAKIonIcons starIconWithSize:size];
    [starIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [starIcon imageWithSize:CGSizeMake(size,size)];
}

+(UIImage *) CommentsImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *commentsIcon = [FAKIonIcons chatbubbleWorkingIconWithSize:size];
    [commentsIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [commentsIcon imageWithSize:CGSizeMake(size,size)];
}
+(UIImage *) ShrinkImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *shrinkIcon = [FAKIonIcons arrowResizeIconWithSize:size];
    [shrinkIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [shrinkIcon imageWithSize:CGSizeMake(size,size)];
}
+(UIImage *) NavigateImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *navigateIcon = [FAKIonIcons ios7NavigateIconWithSize:size];
    [navigateIcon addAttribute:NSForegroundColorAttributeName value:color];
    return [navigateIcon imageWithSize:CGSizeMake(size,size)];
}

+(UIImage *) AlertImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *icon = [FAKIonIcons alertCircledIconWithSize:size];
    [icon addAttribute:NSForegroundColorAttributeName value:color];
    return [icon imageWithSize:CGSizeMake(size,size)];
}

+(UIImage *) HelpImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *icon = [FAKIonIcons helpCircledIconWithSize:size];
    [icon addAttribute:NSForegroundColorAttributeName value:color];
    return [icon imageWithSize:CGSizeMake(size,size)];
}
+(UIImage *) TrashImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *icon = [FAKIonIcons ios7TrashIconWithSize:size];
    [icon addAttribute:NSForegroundColorAttributeName value:color];
    return [icon imageWithSize:CGSizeMake(size,size)];
}
+(UIImage *) FoursquareImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKFontAwesome *icon = [FAKFontAwesome foursquareIconWithSize:size];
    [icon addAttribute:NSForegroundColorAttributeName value:color];
    return [icon imageWithSize:CGSizeMake(size, size)];
}
+(UIImage *) ShareImageWithSize:(float)size withColor:(UIColor *)color
{
    FAKIonIcons *icon = [FAKIonIcons ios7UploadOutlineIconWithSize:size];
    [icon addAttribute:NSForegroundColorAttributeName value:color];
    return [icon imageWithSize:CGSizeMake(size,size)];

}

@end
