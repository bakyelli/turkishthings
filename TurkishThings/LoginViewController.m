//
//  ViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "LoginViewController.h"
#import <Parse/Parse.h>
#import <FacebookSDK/FacebookSDK.h>
#import "VenuesViewController.h"
#import "MenuViewController.h"
#import <MMDrawerController.h>
#import <SVProgressHUD.h>
#import "DataStore.h"
#import "SignupViewController.h"

@interface LoginViewController ()

@property (nonatomic, strong) MMDrawerController *drawerController;
@property (nonatomic, strong) NSMutableData *imageData;
@property (nonatomic,strong) void (^block)(void);

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    PFUser *currentUser = [PFUser currentUser];
    if(currentUser)
    {
        //Already logged in -- continue!
        NSLog(@"This user is already logged in.");
        NSLog(@"This user's username is: %@", currentUser.username);
        [self presentVenues];
    }
    
    [self.navigationController setNavigationBarHidden:YES];
}


- (void) presentVenues
{
    [[DataStore SharedStore] recordUserLastLogin];
    [self performSegueWithIdentifier:@"presentVenues" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"signUpOrLogin"])
    {
        
        UIButton *senderButton = (UIButton *)sender;
        SignupViewController *svc = (SignupViewController *) segue.destinationViewController;

        if(senderButton.tag == 1) //Login
        {
            svc.viewType = @"Login";
        }
        else if(senderButton.tag == 2)//Signup
        {
            svc.viewType = @"Signup";
        }
    }
}

- (IBAction)btnLoginPressed:(id)sender {
    

    
    [self performSegueWithIdentifier:@"signUpOrLogin" sender:sender];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self btnLoginPressed:self];
    return YES;
}

- (IBAction)btnLoginWithFacebookPressed:(id)sender {
    NSArray *permissions = nil;
    [[DataStore SharedStore] showProgressWithText:@"Connecting to Facebook..."];
    [PFFacebookUtils logInWithPermissions:permissions block:^(PFUser *user, NSError *error) {
        if(error)
        {
            NSLog(@"%@", error);
        }
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
            [SVProgressHUD dismiss];
        } else if (user.isNew) {
            
            NSLog(@"User signed up and logged in through Facebook!");
        } else {
            
            NSLog(@"User logged in through Facebook!");
        }
        if(user)
        {
            [[DataStore SharedStore] showProgressWithText:@"Logged in, downloading user data..."];

            FBRequest *request = [FBRequest requestForMe];
            [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if(result){
                    NSDictionary *userDic = (NSDictionary *)result;
                    NSString *fbUserName = userDic[@"name"];
                    
                    if(fbUserName && ![fbUserName isEqualToString:@""])
                        
                    {
                        [self getFacebookProfilePictureWithFacebookID:userDic[@"id"] withCompletion:^{
                            [user addObject:fbUserName forKey:@"userReviewerName"];
                            user[@"profilePicture"] = self.imageData;
                            [user saveInBackground];
                            [self presentVenues];
                            [[DataStore SharedStore] showSuccessProgressWithText:@"Login Successful!"];
                        }];
                    }
                    else
                    {
                        [[DataStore SharedStore] showErrorProgressWithText:@"Error!"];
                    }
                }
                else
                {
                    [[DataStore SharedStore] showErrorProgressWithText:@"Error!"];
                }
            }];
        }
    }];
}
-(void)getFacebookProfilePictureWithFacebookID:(NSString *)facebookID withCompletion:(void (^) (void))completionBlock;
{
 
    NSString *urlString = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", facebookID];
    NSMutableURLRequest *urlRequest =
    [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                        timeoutInterval:2];
    
    NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest
                                                                     delegate:self];
    self.block = completionBlock;
    
    if (!urlConnection)
        NSLog(@"Failed to download picture");
}

#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    self.imageData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.imageData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    self.block();
}

@end
