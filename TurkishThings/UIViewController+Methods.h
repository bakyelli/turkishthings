//
//  UIViewController+Methods.h
//  TurkishThings
//
//  Created by Basar Akyelli on 12/21/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataStore.h"

@interface UIViewController (Methods)
-(void)showActivityIndicator;
-(void)dismissActivityIndicator;

@end
