//
//  LocationSettingsViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 12/2/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "LocationSettingsViewController.h"
#import "LocationSettingsNavigationController.h"
#import "UIViewController+Methods.h"
#import <MapKit/MapKit.h>
#import <UIAlertView+Blocks.h>
@interface LocationSettingsViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *currentLocationSwitcher;
@property (weak, nonatomic) IBOutlet UISearchBar *locationSearchBar;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocation *selectedLocation;
@end

@implementation LocationSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:self.tableView.backgroundColor];
    [self.currentLocationSwitcher addTarget:self action:@selector(switcherStatusChanged:) forControlEvents:UIControlEventValueChanged];
   
    UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:)];
    [self.navigationItem setRightBarButtonItem:doneBarButtonItem];
    
    
    self.currentLocationSwitcher.on = self.useCurrentLocation;
    if(![self.currentLocationSwitcher isOn])
    {
        [self showPlacemarkOnMap:self.vvc.selectedPlacemark];
    }
    [self switcherStatusChanged:nil];
    
}
-(void)showPlacemarkOnMap:(CLPlacemark *)firstPlaceMark
{
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc]init];
    annotation.coordinate = firstPlaceMark.location.coordinate;
    annotation.title = [NSString stringWithFormat:@"%@, %@", firstPlaceMark.locality, firstPlaceMark.country];
    
    [self.mapView addAnnotation:annotation];
    [self.mapView selectAnnotation:annotation animated:YES];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(firstPlaceMark.location.coordinate, 70000, 70000);
    [self.mapView setRegion:region animated:YES];

}
- (void)sendNotificaiton
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Location Settings Changed" object:self];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)donePressed:(id)sender
{
    self.vvc.useCurrentLocation = [self.currentLocationSwitcher isOn];
    
    if(![self.currentLocationSwitcher isOn])
    {
        if(self.selectedLocation == nil)
        {
            [UIAlertView showWithTitle:nil message:@"You need to pick a location or set to use your current location." cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        }
        else{
            self.vvc.currentLocation =self.selectedLocation;
            [self sendNotificaiton];
        }
    }
    else{
        [self sendNotificaiton];
    }
}
-(void)switcherStatusChanged:(id)sender
{
    if([self.currentLocationSwitcher isOn])
    {
        [self.locationSearchBar setUserInteractionEnabled:NO];
        [self.mapView setUserInteractionEnabled:NO];
        self.mapView.alpha = 0.5;
        self.locationSearchBar.alpha = 0.5;
        [self resetMap];
    }
    else
    {
        [self.locationSearchBar setUserInteractionEnabled:YES];
        [self.mapView setUserInteractionEnabled:YES];
        self.mapView.alpha = 1;
        self.locationSearchBar.alpha = 1;
        [self resetMap];

    }
}

-(void)resetMap
{
    MKCoordinateRegion region = MKCoordinateRegionMake(self.mapView.centerCoordinate, MKCoordinateSpanMake(180, 360));
    [self.mapView setRegion:region animated:YES];
}

#pragma mark -UISearchBar Delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self showActivityIndicator];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [searchBar resignFirstResponder];
    CLGeocoder *geocoder = [[CLGeocoder alloc]init];
    [geocoder geocodeAddressString:self.locationSearchBar.text completionHandler:^(NSArray *placemarks, NSError *error) {
        [self dismissActivityIndicator];
        if([placemarks count] > 0)
        {
            CLPlacemark *firstPlaceMark = placemarks[0];
            self.vvc.selectedPlacemark = firstPlaceMark;
            self.selectedLocation = firstPlaceMark.location;
            [self showPlacemarkOnMap:firstPlaceMark];
        }
        else
        {
            [UIAlertView showWithTitle:@"" message:@"Location not found!" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
        }
    }];
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    if([searchBar.text length] > 0)
    {
        [self.locationSearchBar setShowsCancelButton:YES];
    }
    else
    {
        [self.locationSearchBar setShowsCancelButton:NO];
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0) {
        [self.locationSearchBar setShowsCancelButton:NO];
        [searchBar performSelector: @selector(resignFirstResponder)
                        withObject: nil
                        afterDelay: 0.1];
    }
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self.locationSearchBar setShowsCancelButton:NO];
}

@end
