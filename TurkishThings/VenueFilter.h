//
//  VenueFilter.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/22/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VenueCategory.h"
#import <CoreLocation/CoreLocation.h>

@interface VenueFilter : NSObject

@property BOOL filterWithSearchQuery;
@property BOOL filterWithVenueCategory;
@property BOOL filterWithLocation; 

@property (strong, nonatomic) NSString *searchQuery;
@property (strong, nonatomic) VenueCategory *venueCategory; 
@property (strong, nonatomic) CLLocation *location;

@end
