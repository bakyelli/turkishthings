//
//  ColorLibrary.m
//  TurkishThings
//
//  Created by Basar Akyelli on 1/20/14.
//  Copyright (c) 2014 Basar Akyelli. All rights reserved.
//

#import "ColorLibrary.h"
#import "DataStore.h"

@implementation ColorLibrary

+(UIColor *)themeColor
{
    return [[DataStore SharedStore]UIColorFromRGBMethod:0xCC0A24];
}

@end
