//
//  ReportVenue.m
//  TurkishThings
//
//  Created by Basar Akyelli on 12/12/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "ReportVenue.h"

@implementation ReportVenue

-(id) init{
    self = [super init];
    
    if(self)
    {
        self.uniqueID = [[NSProcessInfo processInfo] globallyUniqueString];
    }
    
    return self;
}
-(id) initWithParseObject:(PFObject *)object
{
    self = [super init];
    if (self)
    {
        self.createdByUserName = object[@"createdByUserName"];
        self.createdByUserReviewerName = object[@"createdByUserReviewerName"][0];
        self.uniqueID = object[@"uniqueID"];
        self.storedVenueUniqueID = object[@"storedVenueUniqueID"];        
    }
    return self;
    
}


@end
