//
//  AboutNavigationControllerViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 12/14/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutNavigationControllerViewController : UINavigationController

@end
