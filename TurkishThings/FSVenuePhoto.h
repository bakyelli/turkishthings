//
//  FSVenuePhoto.h
//  TurkishThings
//
//  Created by Basar Akyelli on 12/20/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSVenuePhoto : NSObject

@property (nonatomic, strong) NSString *photoID;
@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSString *prefix;
@property (nonatomic, strong) NSString *suffix;
@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSURL *thumbnailURL;
@property (nonatomic, strong) NSURL *bannerURL;
@property (nonatomic, strong) NSURL *fullsizeURL;

+(NSArray *)convertToPhotos:(NSArray *)photos withThumbnailSize:(NSInteger)size withBannerWidth:(NSInteger)bannerWidth withBannerHeight:(NSInteger)bannerHeight;

@end
