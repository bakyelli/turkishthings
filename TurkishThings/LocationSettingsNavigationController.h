//
//  LocationSettingsNavigationController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 12/4/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VenuesViewController.h"
@interface LocationSettingsNavigationController : UINavigationController

@property (strong, nonatomic) VenuesViewController *vvc; 
@property (assign) BOOL useCurrentLocation;
@property (strong, nonatomic) CLLocation *currentLocation;
@end
