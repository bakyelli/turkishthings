//
//  SignupViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "SignupViewController.h"
#import <Parse/Parse.h>
#import <UIAlertView+Blocks.h>
#import "DataStore.h"

@interface SignupViewController ()

@end

@implementation SignupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    
    if([self.viewType isEqualToString:@"Signup"])
    {
        [self.btnSignup setImage:[UIImage imageNamed:@"signupBtn2.png"] forState:UIControlStateNormal];
    }
    else if([self.viewType isEqualToString:@"Login"])
    {
        [self.btnSignup setImage:[UIImage imageNamed:@"loginBtn.png"] forState:UIControlStateNormal];
        [self.tbSignupPasswordTwo setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)btnSignupPressed:(id)sender {
    
    
    if([self.viewType isEqualToString:@"Signup"])
    {
        PFUser *user = [PFUser user];
        NSString *password = self.tbSignupPasswordOne.text;
        NSString *passwordVerification = self.tbSignupPasswordTwo.text;
        
        user.username = self.tbSignupUsername.text;
        user.password = password;
        
        
        if([password isEqualToString:passwordVerification])
        {
            
            if([self validateInput:self.tbSignupUsername.text withMinLength:5 withMaxLength:15])
            {
                if([self validateInput:self.tbSignupPasswordOne.text withMinLength:6 withMaxLength:15])
                {
                    
                    
                    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        
                        if(!error)
                        {
                            NSLog(@"Sign up successful!");
                            [user addObject:user.username forKey:@"userReviewerName"];
                            [user saveInBackground];
                            [self performSegueWithIdentifier:@"presentVenues" sender:self];
                        }
                        else{
                            NSString *errorString = [error userInfo][@"error"];
                            UIAlertView *errorAlertView = [[UIAlertView alloc]initWithTitle:@"Error" message:errorString delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            [errorAlertView show];
                        }
                    }];
                }
                else
                {
                    UIAlertView *errorAlertView = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Your password should be 6-15 characters in length." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [errorAlertView show];
                }
            }
            else
            {
                UIAlertView *errorAlertView = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Your username should be 5-15 characters in length." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [errorAlertView show];
            }
        }
        else
        {
            UIAlertView *errorAlertView = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Passwords do not match!" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [errorAlertView show];
        }

    }
    else if([self.viewType isEqualToString:@"Login"])
    {
            [[DataStore SharedStore] showProgressWithText:@"Connecting..."];
            [PFUser logInWithUsernameInBackground:self.tbSignupUsername.text password:self.tbSignupPasswordOne.text block:^(PFUser *user, NSError *error) {
                if(user)
                {
                
                    //Authenticated!
                    NSLog(@"Login successful!");
                    NSLog(@"The user's username is: %@", user.username);
        
                    [user addObject:user.username forKey:@"userReviewerName"];
                    [user saveInBackground];
        
                    [self presentVenues];
                    [[DataStore SharedStore] showSuccessProgressWithText:@"Login Successful!"];
        
                }
                else
                {
                    [[DataStore SharedStore] showErrorProgressWithText:@"Error!"];
                    UIAlertView *errorAlertView = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Login failed" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [errorAlertView show];
                }
            }];
    }
    
   }
-(BOOL) validateInput:(NSString *)input withMinLength:(NSInteger)minLength withMaxLength:(NSInteger)maxLength
{
    if(input.length >= minLength && input.length <= maxLength)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (IBAction)btnCancelPressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}
- (void) presentVenues
{
    [[DataStore SharedStore] recordUserLastLogin];
    [self performSegueWithIdentifier:@"presentVenues" sender:self];
}
@end
