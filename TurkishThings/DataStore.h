//
//  DataStore.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoredVenue.h"
#import "StoredVenueReview.h"
#import "FSVenue.h"
#import "VenueFilter.h"
#import <SVProgressHUD.h>
#import "StoredVenueImage.h"
#import "ReportVenue.h"

@interface DataStore : NSObject



+ (DataStore *)SharedStore;
@property (strong, nonatomic) NSMutableArray *venueCategories;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property BOOL isReachable; 
-(void)getAvailableCategoriesWithCompletion:(void (^)(void))completion;
-(void)saveStoredVenueToCloud:(StoredVenue *)venue completionBlock:(void (^)(BOOL))block;
-(void)saveStoredVenueReviewToCloud:(StoredVenueReview *)venueReview completionBlock:(void (^)(BOOL))block;
-(void)saveStoredVenueImageToCloud:(StoredVenueImage *)venueImage completionBlock:(void (^)(BOOL))block;

-(void)getStoredVenuesForACity:(NSString *)city withCompletion:(void (^) (NSArray *))block;
-(void)getStoredVenuesWithVenueFilter:(VenueFilter *)filter withCompletion:(void (^) (NSMutableArray *))block;
-(void)getStoredVenueReviewsForStoredVenueID:(NSString *)storedVenueUniqueID withCompletion:(void (^) (NSMutableArray *))block;
-(void)getAllStoredVenueImageThumbnailsForStoredVenueID:(NSString *)storedVenueUniqueID withCompletion:(void (^) (NSMutableArray *))block;
-(void)getVenueFullSizedImageWithUniqueID:(NSString *)venueImageUniqueID withCompletion:(void (^) (StoredVenueImage *))block;

-(void) fsVenueAlreadyExists:(FSVenue *)venue completionBlock:(void (^)(StoredVenue *))block;

-(void)showSuccessProgressWithText:(NSString *)text;
-(void)showErrorProgressWithText:(NSString *)text;
-(void)showProgressWithText:(NSString *)text;

-(UIColor *)UIColorFromRGBMethod:(unsigned long int)rgbValue;

-(void)reportVenueWithReport:(ReportVenue *)reportVenue;

-(void) deleteStoredVenueWithParseObject:(PFObject *)storedVenue;
-(void) deleteStoredVenueReviewWithParseObject:(PFObject *)storedVenueReview;
-(void)launchFoursquareWithVenueID:(NSString *)venueID;
- (void) setNavBarStyle;
- (void)recordUserLastLogin;




@end
