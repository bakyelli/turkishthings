//
//  FISTableView.m
//  asdf
//
//  Created by Joe Burgess on 11/20/13.
//  Copyright (c) 2013 Joe Burgess. All rights reserved.
//

#import "FISTableView.h"

@implementation FISTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    id hitView = [super hitTest:point withEvent:event];
    if (point.y<0) {
        return nil;
    }
    return hitView;
}

@end
