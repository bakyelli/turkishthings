//
//  LocationSettingsViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 12/2/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VenuesViewController.h"

@interface LocationSettingsViewController : UITableViewController<UISearchBarDelegate>

@property(strong,nonatomic) VenuesViewController *vvc; 
@property (assign) BOOL useCurrentLocation;
@property (strong, nonatomic) CLLocation *currentLocation;

@end
