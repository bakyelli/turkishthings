//
//  AboutViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 12/14/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "AboutViewController.h"
#import <MessageUI/MessageUI.h>
#import <UIAlertView+Blocks.h>
#import "WebViewViewController.h"
#import <FISWebViewPreloader.h>
#import "StoredVenue.h"

@interface AboutViewController ()<MFMailComposeViewControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleVersionLabel;
- (IBAction)sendEmail:(id)sender;
- (IBAction)goToFlatironSchool:(id)sender;
- (IBAction)btnMadeInNycClicked:(id)sender;
- (IBAction)btnAzizClicked:(id)sender;
@property (strong,nonatomic) FISWebViewPreloader *preLoader;
@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setupPreloaders
{
    self.preLoader = [FISWebViewPreloader new];
    [self.preLoader setURLString:@"http://www.flatironschool.com" forKey:@"FIS" withCGRect:self.view.bounds];
    [self.preLoader setURLString:@"http://www.wearemadeinny.com" forKey:@"NY" withCGRect:self.view.bounds];
    [self.preLoader setURLString:@"http://www.azizk.com" forKey:@"AzizK" withCGRect:self.view.bounds];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *doneBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:)];
    [self.navigationItem setRightBarButtonItem:doneBarButtonItem];
    
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: @"" style: UIBarButtonItemStylePlain target: nil action: nil];
    [[self navigationItem] setBackBarButtonItem: newBackButton];

    [self setupPreloaders];
    
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    
    self.titleVersionLabel.text = [NSString stringWithFormat:@"Turkish Things %@",version];

}

-(void)donePressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendEmail:(id)sender {
    // You must check that the current device can send email messages before you
    // attempt to create an instance of MFMailComposeViewController.  If the
    // device can not send email messages,
    // [[MFMailComposeViewController alloc] init] will return nil.  Your app
    // will crash when it calls -presentViewController:animated:completion: with
    // a nil view controller.
    if ([MFMailComposeViewController canSendMail])
        // The device can send email.
    {
        [self displayMailComposerSheet];
    }
    else
        // The device can not send email.
    {
        [UIAlertView showWithTitle:nil message:@"Can't send e-mail from this device!" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
    }

}

- (IBAction)goToFlatironSchool:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    WebViewViewController *webViewVC = [storyboard instantiateViewControllerWithIdentifier:@"showWebView"];
    webViewVC.venuePreLoadedWebView =[self.preLoader webViewForKey:@"FIS"];

    StoredVenue *venue = [[StoredVenue alloc]init];
    venue.title = @"Flatiron School";
    webViewVC.storedVenue = venue;
    
    
    [self.navigationController pushViewController:webViewVC animated:YES];
}

- (IBAction)btnMadeInNycClicked:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    WebViewViewController *webViewVC = [storyboard instantiateViewControllerWithIdentifier:@"showWebView"];
    webViewVC.venuePreLoadedWebView =[self.preLoader webViewForKey:@"NY"];
    
    StoredVenue *venue = [[StoredVenue alloc]init];
    venue.title = @"We Are Made in NY";
    webViewVC.storedVenue = venue;
    
    
    [self.navigationController pushViewController:webViewVC animated:YES];
}

- (IBAction)btnAzizClicked:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    WebViewViewController *webViewVC = [storyboard instantiateViewControllerWithIdentifier:@"showWebView"];
    webViewVC.venuePreLoadedWebView =[self.preLoader webViewForKey:@"AzizK"];
    
    StoredVenue *venue = [[StoredVenue alloc]init];
    venue.title = @"Aziz K";
    webViewVC.storedVenue = venue;
    
    
    [self.navigationController pushViewController:webViewVC animated:YES];

}

- (void)displayMailComposerSheet
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	
    picker.mailComposeDelegate = self;
	NSArray *toRecipients = [NSArray arrayWithObject:@"tt@basarakyelli.com"];
	
	[picker setToRecipients:toRecipients];
    [self presentViewController:picker animated:YES completion:NULL];

}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	[self dismissViewControllerAnimated:YES completion:NULL];
}

@end
