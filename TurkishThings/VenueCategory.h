//
//  VenueCategory.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VenueCategory : NSObject

@property (nonatomic) NSString *categoryName; 

-(id) initWithName:(NSString *)name;
+(UIImage *)categoryIconForCategoryString:(NSString *)categoryName withSize:(CGFloat)size withColor:(UIColor *)color;

@end
