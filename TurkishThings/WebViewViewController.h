//
//  WebViewViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 12/2/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoredVenue.h"

@interface WebViewViewController : UIViewController

@property (strong, nonatomic) UIWebView *venuePreLoadedWebView;
@property (strong, nonatomic) StoredVenue *storedVenue; 
@end
