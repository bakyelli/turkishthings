//
//  transitionViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/24/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "transitionViewController.h"
#import "VenuesViewController.h"
#import "MenuViewController.h"
#import "menuNavigationViewController.h"

@interface transitionViewController ()

@end

@implementation transitionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:NO];
    self.navigationItem.hidesBackButton = YES;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    menuNavigationViewController *vvc = [storyboard instantiateViewControllerWithIdentifier:@"navController"];
    MenuViewController *mvc = [storyboard instantiateViewControllerWithIdentifier:@"menuController"];
    self.drawerController = [[MMDrawerController alloc]initWithCenterViewController:vvc leftDrawerViewController:mvc];
    
    [self.drawerController setRestorationIdentifier:@"MMDrawer"];
    [self.drawerController setMaximumLeftDrawerWidth:200];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    [self presentViewController:self.drawerController animated:NO completion:nil];
}


@end
