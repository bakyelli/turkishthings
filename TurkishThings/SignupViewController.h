//
//  SignupViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignupViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *tbSignupUsername;
@property (weak, nonatomic) IBOutlet UITextField *tbSignupPasswordOne;
@property (weak, nonatomic) IBOutlet UITextField *tbSignupPasswordTwo;
@property (strong, nonatomic) NSString *viewType; 
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;


- (IBAction)btnSignupPressed:(id)sender;
- (IBAction)btnCancelPressed:(id)sender;

@end
