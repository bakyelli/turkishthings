//
//  AppDelegate.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import <Foursquare2.h>
#import <SVProgressHUD.h>
#import <Reachability/Reachability.h>
#import <Crashlytics/Crashlytics.h>


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.executionType = AppExecutionTypeProd;
    
    if(self.executionType == AppExecutionTypeDev)
    {
        [Parse setApplicationId:@"qYSHLfyGB4pzc9JCKUE4SwGt7tw9ymwk5zRVP1MJ" clientKey:@"WoTvRiAldjePpCTkRs70igE0FIGtiduPlEBuprtU"];
    }
    else if(self.executionType == AppExecutionTypeProd)
    {
        [Parse setApplicationId:@"G1gURnz9QCmPTzza65NRrsjlLKLGNFBhanLSA6Yw" clientKey:@"kHbYvIszS2YnwYjfMKfdCB5aqizsNTTkmR08cMGt"];
    }
    
    [PFFacebookUtils initializeFacebook];
    
    
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
  
    [Foursquare2 setupFoursquareWithClientId:@"1XRBSMKNJMFYPZZPWCAIKY2I0PMHPBZ1BW0BTGTJ1TBY02ZY"
                                      secret:@"BQKTAEIHW4WHA21G3UCH03TBTHZSWM223Z4QBHRRXIJQA1T2"
                                 callbackURL:@"turkishthings://foursquare"];
    

    [[DataStore SharedStore] setNavBarStyle];
    
    
    Reachability *reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    reach.reachableOnWWAN = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];

    [reach startNotifier];
    
    [Crashlytics startWithAPIKey:@"b24b7d75ec140d52293ae1c1c1dec148385e88fc"];
    
    return YES;
}

-(void)reachabilityChanged:(NSNotification *)notification
{
    Reachability *reach = (Reachability *)notification.object;
    if(!reach.isReachable)
    {
        [DataStore SharedStore].isReachable = NO;
        [SVProgressHUD dismiss];
        [[DataStore SharedStore] showErrorProgressWithText:@"Lost network connection!"];
    }
    else
    {
        [DataStore SharedStore].isReachable = YES;

    }
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [PFFacebookUtils handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [PFFacebookUtils handleOpenURL:url];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
  //  [SVProgressHUD dismiss];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
