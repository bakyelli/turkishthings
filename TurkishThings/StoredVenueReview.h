//
//  StoredVenueReview.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/8/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface StoredVenueReview : NSObject

@property (strong,nonatomic) NSString *uniqueID;
@property (strong,nonatomic) NSString *storedVenueUniqueID;
@property (strong,nonatomic) NSString *createdByUserName;
@property (strong,nonatomic) NSString *createdByUserReviewerName;
@property (strong,nonatomic) NSNumber *rating;
@property (strong,nonatomic) NSString *review;
@property (strong,nonatomic) NSDate *createdAt;
@property (strong,nonatomic) PFObject *parseObject;
-(id) initWithParseObject:(PFObject *)object;


@end
