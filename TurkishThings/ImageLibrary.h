//
//  ImageLibrary.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/28/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageLibrary : NSObject

+(UIImage *) ProfilePicturePlaceholderImageWithSize:(float)size;
+(UIImage *) ProfilePictureImageWithPlaceHolderSize:(float)size;
+(UIImage *) MenuSettingsLogOffIconImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) MapsImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) ImagesImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) DismissImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) HyperlinkImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) MenuSettingsLocationIconImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) StarImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) CommentsImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) ShrinkImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) NavigateImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) AlertImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) HelpImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) TrashImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) FoursquareImageWithSize:(float)size withColor:(UIColor *)color;
+(UIImage *) ShareImageWithSize:(float)size withColor:(UIColor *)color;

@end
