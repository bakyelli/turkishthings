//
//  ColorLibrary.h
//  TurkishThings
//
//  Created by Basar Akyelli on 1/20/14.
//  Copyright (c) 2014 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorLibrary : NSObject

+(UIColor *) themeColor; 

@end
