//
//  LocationSettingsNavigationController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 12/4/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "LocationSettingsNavigationController.h"
#import "LocationSettingsViewController.h"
@interface LocationSettingsNavigationController ()

@end

@implementation LocationSettingsNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    LocationSettingsViewController *lsvc = (LocationSettingsViewController *)self.viewControllers[0];
    lsvc.vvc = self.vvc;
    
    lsvc.useCurrentLocation = self.useCurrentLocation;
    lsvc.currentLocation = self.currentLocation;
    
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
