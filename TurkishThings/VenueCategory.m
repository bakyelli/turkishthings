//
//  VenueCategory.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "VenueCategory.h"
#import <Parse/Parse.h>
#import "DataStore.h"
#import <FontAwesomeKit.h>


@implementation VenueCategory

-(id) initWithName:(NSString *)name{
    self = [super init];
    
    if(self)
    {
        self.categoryName = name;
    }
    
    return self;
}
+(UIImage *)categoryIconForCategoryString:(NSString *)categoryName withSize:(CGFloat)size withColor:(UIColor *)color
{
    if([categoryName  isEqual: @"Restaurant"])
    {
        FAKFontAwesome *icon = [FAKFontAwesome cutleryIconWithSize:size];
        [icon addAttribute:NSForegroundColorAttributeName value:color];
        return [icon imageWithSize:CGSizeMake(size,size)];

    }
    else if([categoryName  isEqual: @"Health"])
    {
        FAKFontAwesome *icon = [FAKFontAwesome medkitIconWithSize:size];
        [icon addAttribute:NSForegroundColorAttributeName value:color];
        return [icon imageWithSize:CGSizeMake(size,size)];

    }
    else if([categoryName  isEqual: @"Nightlife"])
    {
        FAKFontAwesome *icon = [FAKFontAwesome glassIconWithSize:size];
        [icon addAttribute:NSForegroundColorAttributeName value:color];
        return [icon imageWithSize:CGSizeMake(size,size)];
    }
    else if([categoryName  isEqual: @"Grocery Shopping"])
    {
        FAKFontAwesome *icon = [FAKFontAwesome shoppingCartIconWithSize:size];
        [icon addAttribute:NSForegroundColorAttributeName value:color];
        return [icon imageWithSize:CGSizeMake(size,size)];
    }
    else
    {
        FAKFontAwesome *icon = [FAKFontAwesome mapMarkerIconWithSize:size];
        [icon addAttribute:NSForegroundColorAttributeName value:color];
        return [icon imageWithSize:CGSizeMake(size,size)];
    }
    
}


@end
