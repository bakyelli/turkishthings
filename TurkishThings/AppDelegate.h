//
//  AppDelegate.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataStore.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

typedef enum
{
    AppExecutionTypeDev,
    AppExecutionTypeProd
} AppExecutionType;


@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) AppExecutionType executionType;

@end
