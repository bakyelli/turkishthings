//
//  menuNavigationViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/24/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "menuNavigationViewController.h"

@interface menuNavigationViewController ()

@end

@implementation menuNavigationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
