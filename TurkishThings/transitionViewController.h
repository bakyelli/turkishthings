//
//  transitionViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/24/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MMDrawerController/MMDrawerController.h>

@interface transitionViewController : UIViewController

@property (strong,nonatomic) MMDrawerController *drawerController;

@end
