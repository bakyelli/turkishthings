//
//  MenuViewController.h
//  
//
//  Created by Basar Akyelli on 11/8/13.
//
//

#import <UIKit/UIKit.h>
#import "VenuesViewController.h"

@interface MenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) VenuesViewController *venuesViewController; 
@end
