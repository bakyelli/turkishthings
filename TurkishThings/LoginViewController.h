//
//  ViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate, NSURLConnectionDataDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tbLoginUsername;
@property (weak, nonatomic) IBOutlet UITextField *tbLoginPassword;

- (IBAction)btnLoginPressed:(id)sender;
- (IBAction)btnLoginWithFacebookPressed:(id)sender;


@end
