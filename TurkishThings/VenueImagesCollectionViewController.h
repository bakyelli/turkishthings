//
//  VenueImagesCollectionViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/30/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoredVenue.h"
#import <FSImageViewer/FSBasicImage.h>
#import <FSImageViewer/FSBasicImageSource.h>
#import <FSImageViewer/FSImageViewerViewController.h>

@interface VenueImagesCollectionViewController : UICollectionViewController

@property (strong,nonatomic) StoredVenue *venue;
@property (strong,nonatomic) NSMutableArray *imagesArray;
@property (strong,nonatomic) NSMutableArray *imageArrayForSource;
@property (strong,nonatomic) FSBasicImageSource *basicSource;

@end
