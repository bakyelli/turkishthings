//
//  NewVenueDetailsTableViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSVenue.h"
#import <EDStarRating.h>
#import <MapKit/MapKit.h>
#import "StoredVenue.h"

@interface NewVenueDetailsTableViewController : UITableViewController<UIActionSheetDelegate, EDStarRatingProtocol, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tbVenueName;
@property (weak, nonatomic) IBOutlet UITextView *tbVenueReview;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet EDStarRating *starRatingImage;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;

@property (strong, nonatomic) FSVenue *venue;
@property (strong, nonatomic) StoredVenue *existingVenue;
@property (assign) BOOL isExistingVenue;

@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnDeletePicture;

- (IBAction)btnDeletePicturePressed:(id)sender;

- (IBAction)btnPickCategoryPressed:(id)sender;
- (void)btnSubmitPressed;
- (IBAction)btnCancelPressed:(id)sender;
- (IBAction)btnAddPicturePressed:(id)sender;
- (IBAction)btnCameraBtnPressed:(id)sender;

@end
