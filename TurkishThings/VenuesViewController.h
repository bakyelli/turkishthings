//
//  VenuesViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <EDStarRating.h>
#import "VenueCategory.h"
#import "VenueFilter.h"

@interface VenuesViewController : UIViewController <MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *venues;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) CLPlacemark *selectedPlacemark;
@property (assign) BOOL useCurrentLocation; 

-(void)menuButtonPressed:(id)sender;
-(void)categoryPressed:(VenueCategory *)selectedCategory;
-(void) populateVenuesWithFilter:(VenueFilter *)filter;
@end
