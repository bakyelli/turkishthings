//
//  UIViewController+Methods.m
//  TurkishThings
//
//  Created by Basar Akyelli on 12/21/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "UIViewController+Methods.h"

@implementation UIViewController (Methods)
-(void)showActivityIndicator
{
    [DataStore SharedStore].activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:[DataStore SharedStore].activityIndicator ];
    if(self.navigationItem.rightBarButtonItem != nil)
    {
        NSArray *tempArray = [NSArray arrayWithObjects:self.navigationItem.rightBarButtonItem, barButton, nil];
        [self.navigationItem setRightBarButtonItems:tempArray];
    }
    else
    {
        [self.navigationItem setRightBarButtonItem:barButton];
    }
    [[DataStore SharedStore].activityIndicator startAnimating];
}

-(void)dismissActivityIndicator
{
    [SVProgressHUD dismiss];
    [[DataStore SharedStore].activityIndicator stopAnimating];
}
@end
