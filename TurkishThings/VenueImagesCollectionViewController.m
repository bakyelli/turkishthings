//
//  VenueImagesCollectionViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/30/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "VenueImagesCollectionViewController.h"
#import "DataStore.h"
#import "StoredVenueImage.h"
#import "ImageLibrary.h"
#import <UIAlertView+Blocks.h>
#import <Foursquare2.h>
#import "FSVenuePhoto.h"
#import <UIImageView+AFNetworking.h>


@interface VenueImagesCollectionViewController ()

@end

@implementation VenueImagesCollectionViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(self.imagesArray == nil)
    {
        [self setNoImages];
    }

}

- (void)setNoImages
{
    UIImage *noImagesImage = [ImageLibrary ImagesImageWithSize:75 withColor:[UIColor lightGrayColor]];
    UIImageView *noImagesImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, 75, 75)];
    noImagesImageView.center = self.collectionView.center;
    [noImagesImageView setImage:noImagesImage];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(noImageTapped:)];
    [singleTap setNumberOfTapsRequired:1];
    
    [self.collectionView addSubview:noImagesImageView];
    [self.collectionView addGestureRecognizer:singleTap];
}

-(void)noImageTapped:(id)sender
{
   [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIToolbar *toolBar;
    CGRect rect2 = CGRectMake(0,  [[UIScreen mainScreen] bounds].size.height - 44 , self.view.frame.size.width , 0);
    toolBar = [[UIToolbar alloc]initWithFrame:rect2];
    
    [toolBar setTranslucent:NO];
    [toolBar setBarTintColor:[UIColor darkGrayColor]];
    
    [toolBar sizeToFit];
    
    UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc]initWithImage:[ImageLibrary DismissImageWithSize:25 withColor:[UIColor blackColor]] style:UIBarButtonItemStylePlain target:self action:@selector(dismissButtonPressed:)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil ];
    UIBarButtonItem *foursquareButtonIcon = [[UIBarButtonItem alloc]initWithImage:[ImageLibrary FoursquareImageWithSize:25 withColor:[UIColor blackColor]] style:UIBarButtonItemStylePlain target:self action:@selector(foursquareButtonPressed:)];
    UIBarButtonItem *foursquareButton = [[UIBarButtonItem alloc]initWithTitle:@"Add more pictures using Foursquare!" style:UIBarButtonItemStylePlain target:self action:@selector(foursquareButtonPressed:)];
    [foursquareButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Avenir" size:10.0], NSFontAttributeName, nil] forState:UIControlStateNormal];

    [toolBar setItems:@[dismissButton, flex,foursquareButton,foursquareButtonIcon]];
    [self.view addSubview:toolBar];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

}

-(void)foursquareButtonPressed:(id)sender
{
    [UIAlertView showWithTitle:nil message:@"You will leave Turkish Things to launch the Foursquare app." cancelButtonTitle:@"No" otherButtonTitles:@[@"Ok"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1)
        {
            [[DataStore SharedStore]launchFoursquareWithVenueID:self.venue.foursquareVenueID];
        }
    }];
    
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [super viewWillDisappear:animated];
}
-(void)dismissButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5,5,5);
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if(section == 0) //usersubmitted images
    {
        return 0;
    }
    else //foursquare images
    {
        return [self.imagesArray count];

    }
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    UIImageView *venueImage = (UIImageView *)[cell viewWithTag:1];

    if(indexPath.section == 0)
    {
        return cell;
    }
    else if(indexPath.section == 1)
    {
        UIImage *noImagesImage = [ImageLibrary ImagesImageWithSize:50 withColor:[UIColor lightGrayColor]];
        FSVenuePhoto *venuePhoto = self.imagesArray[indexPath.row];
        [venueImage setImageWithURL:venuePhoto.thumbnailURL placeholderImage:noImagesImage];
        
        return cell;
    }
    return nil;
}

  
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[DataStore SharedStore]showProgressWithText:@"Downloading image..."];

    if(indexPath.section == 0)
    {
        
    }
    else if(indexPath.section == 1)
    {
        FSImageViewerViewController *imageViewController = [[FSImageViewerViewController alloc]initWithImageSource:self.basicSource imageIndex:indexPath.row];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imageViewController];
        [imageViewController setTitle:self.venue.title];
        [self presentViewController:navigationController animated:YES completion:^{
            [imageViewController setTitle:self.venue.title];
        }];
        
    }
    
    [SVProgressHUD dismiss];
}

@end
