//
//  StoredVenueRating.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/7/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoredVenueRating : NSObject

@property (strong, nonatomic) NSNumber *rating;

@end
