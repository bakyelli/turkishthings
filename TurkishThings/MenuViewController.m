//
//  MenuViewController.m
//  
//
//  Created by Basar Akyelli on 11/8/13.
//
//

#import "MenuViewController.h"
#import <Parse/Parse.h>
#import "RootNavigationController.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "DataStore.h"
#import "VenuesViewController.h"
#import <UIAlertView+Blocks.h>
#import "ImageLibrary.h"
#import "LocationSettingsViewController.h"
#import "LocationSettingsNavigationController.h"
#import "AboutNavigationControllerViewController.h"
#import <Reachability.h>
#import <UIActionSheet+Blocks.h>
#import <UIAlertView+Blocks.h>
@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    [self preparePictureView];
    [self populateCategories];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:kReachabilityChangedNotification object:nil];

}

- (void)populateCategories
{
    [[DataStore SharedStore]getAvailableCategoriesWithCompletion:^{
        [self.tableView reloadData];
    }];
}
-(void)receivedNotification:(NSNotification *)notification
{
    if([[notification name] isEqualToString:kReachabilityChangedNotification])
    {
        Reachability *reach = (Reachability *)notification.object;
        if(reach.isReachable)
        {
            [self populateCategories];
        }
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:animated];
    [self.tableView setBackgroundColor:[UIColor darkGrayColor]];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(![self hasFourInchDisplay])
    {
        [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, 450.0f)];
    }
}

- (BOOL)hasFourInchDisplay {
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0);
}

-(void)preparePictureView
{
    [self.tableView setContentInset:UIEdgeInsetsMake(150, 0, 0, 0)];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(50, -140, 100, 100)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    imageView.image = [ImageLibrary ProfilePictureImageWithPlaceHolderSize:100];

    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 50.0;
    imageView.layer.borderColor = [UIColor blackColor].CGColor;
    imageView.layer.borderWidth = 3.0f;
    imageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    imageView.layer.shouldRasterize = YES;
    imageView.clipsToBounds = YES;
    imageView.contentMode = UIViewContentModeScaleAspectFill;

    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 110, 0, 24)];
    label.text =[PFUser currentUser][@"userReviewerName"][0];
    label.font = [UIFont fontWithName:@"Avenir" size:10];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor lightGrayColor];
    [label sizeToFit];
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    label.center = imageView.center;
    [label setFrame:CGRectMake(label.frame.origin.x, label.frame.origin.y+65, label.frame.size.width, label.frame.size.height)];
    [view addSubview:imageView];
    [view addSubview:label];
    
    [imageView setUserInteractionEnabled:YES];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(pictureTapped:)];
    [singleTap setNumberOfTapsRequired:1];
    [imageView addGestureRecognizer:singleTap];

    
    [self.tableView addSubview:view];
}

-(void)pictureTapped:(UIGestureRecognizer *)recognizer
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    [self logOff:storyboard];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) //settings header
    {
        return 18;
    }
    if (indexPath.section == 2) //categories header
    {
        return 18;
    }
       return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0) //settings header
    {
        return 1;
    }
    if (section == 1) //settings section
    {
        return 3;
    }
    if (section == 2) //categories header
    {
        return 1;
    }
    if (section == 3) //categories section
    {
        return [[DataStore SharedStore].venueCategories count] + 1;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *settingsCellIdentifier = @"cell";
    static NSString *categoriesCellIdentifier = @"categoryCell";
    static NSString *headerCellIdentifier = @"headerCell";

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:settingsCellIdentifier];
    
    if(indexPath.section == 0) //Settings header
    {
        cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier];
        UILabel *headerTitleLabel = (UILabel *)[cell.contentView viewWithTag:1];
        headerTitleLabel.text = @"Settings";
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }
    else if(indexPath.section == 1) //Settings section
    {
        //        cell = [tableView dequeueReusableCellWithIdentifier:settingsCellIdentifier];
        //        cell.textLabel.text = @"Log Off";
        if(indexPath.row ==0){
            
            cell = [tableView dequeueReusableCellWithIdentifier:categoriesCellIdentifier];
            UIImageView *settingsIconImageView = (UIImageView *)[cell.contentView viewWithTag:1];
            UILabel *settingsLabel = (UILabel *)[cell.contentView viewWithTag:2];
            settingsLabel.text = @"Log Off";
            settingsIconImageView.image = [ImageLibrary MenuSettingsLogOffIconImageWithSize:18 withColor:[UIColor lightGrayColor]];
        }
        else if(indexPath.row == 1)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:categoriesCellIdentifier];
            UIImageView *settingsIconImageView = (UIImageView *)[cell.contentView viewWithTag:1];
            UILabel *settingsLabel = (UILabel *)[cell.contentView viewWithTag:2];
            settingsLabel.text = @"Location Settings";
            settingsIconImageView.image = [ImageLibrary MenuSettingsLocationIconImageWithSize:18 withColor:[UIColor lightGrayColor]];
        }
        else if(indexPath.row == 2)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:categoriesCellIdentifier];
            UIImageView *settingsIconImageView = (UIImageView *)[cell.contentView viewWithTag:1];
            UILabel *settingsLabel = (UILabel *)[cell.contentView viewWithTag:2];
            settingsLabel.text = @"About";
            settingsIconImageView.image = [ImageLibrary HelpImageWithSize:18 withColor:[UIColor lightGrayColor]];
        }

        
    }
    else if(indexPath.section == 2) // Categories Header
    {
        cell = [tableView dequeueReusableCellWithIdentifier:headerCellIdentifier];
        UILabel *headerTitleLabel = (UILabel *)[cell.contentView viewWithTag:1];
        headerTitleLabel.text = @"Venue Categories";
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }
    else if(indexPath.section == 3)// Categories section
    {
        cell = [tableView dequeueReusableCellWithIdentifier:categoriesCellIdentifier];
        UIImageView *categoryImageView = (UIImageView *)[cell.contentView viewWithTag:1];
        UILabel *categoryTitleLabel = (UILabel *)[cell.contentView viewWithTag:2];

        if(indexPath.row + 1 <= [[DataStore SharedStore].venueCategories count]){
            
        NSMutableArray *categoriesArray = [DataStore SharedStore].venueCategories;
        VenueCategory *newCategory = categoriesArray[indexPath.row];
        categoryTitleLabel.text = newCategory.categoryName;
        categoryImageView.image = [VenueCategory categoryIconForCategoryString:newCategory.categoryName withSize:18 withColor:[UIColor lightGrayColor]];
        }
        else
        {
            categoryTitleLabel.text = @"All Venues";
            categoryImageView.image = [VenueCategory categoryIconForCategoryString:@"other" withSize:18 withColor:[UIColor lightGrayColor]];
        }
    }
    
    return cell;
}


- (void)logOff:(UIStoryboard *)storyboard
{
    [UIAlertView showWithTitle:@"Confirmation" message:@"Are you sure you'd like to log off?" cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == [alertView cancelButtonIndex])
        {
            [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
        }
        else
        {
            RootNavigationController *rnc = [storyboard instantiateViewControllerWithIdentifier:@"contentController"];
            [PFUser logOut];
            [[PFUser currentUser] removeObjectForKey:@"userReviewerName"];
            [[PFUser currentUser] removeObjectForKey:@"profilePicture"];
            [[PFUser currentUser] setObject:nil forKey:@"userReviewerName"];
            [[PFUser currentUser] setObject:nil forKey:@"profilePicture"];
            [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
            [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
            [self.mm_drawerController setCenterViewController:rnc withCloseAnimation:(YES) completion:^(BOOL finished) {
            }];
            
        }
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    if(indexPath.section == 1 && indexPath.row == 0) //log off
    {
        [self logOff:storyboard];
    }
    else if(indexPath.section == 1 && indexPath.row == 1) //location settings
    {
        LocationSettingsNavigationController *lsnc =[storyboard instantiateViewControllerWithIdentifier:@"locationSettingsNC"];
        lsnc.vvc = self.venuesViewController;
        lsnc.useCurrentLocation = self.venuesViewController.useCurrentLocation;
        lsnc.currentLocation = self.venuesViewController.currentLocation;
    
        
        [self.venuesViewController presentViewController:lsnc animated:YES completion:nil];
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];

    }
    else if (indexPath.section == 1 && indexPath.row == 2) //about
    {
        AboutNavigationControllerViewController *ancvc =[storyboard instantiateViewControllerWithIdentifier:@"aboutNC"];
        
        [self.venuesViewController presentViewController:ancvc animated:YES completion:nil];
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];

    }
    else if(indexPath.section == 3) //Categories
    {
        if(indexPath.row +1 <= [[DataStore SharedStore].venueCategories count]){
        VenueCategory *selectedCategory = [DataStore SharedStore].venueCategories[indexPath.row];
        [self.venuesViewController categoryPressed:selectedCategory];
        }
        else // "all venues" button
        {
            VenueFilter *filter = [VenueFilter new];
            filter.filterWithLocation = YES;
            filter.location = self.venuesViewController.currentLocation;
            [self.venuesViewController populateVenuesWithFilter:filter];
        }
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];

    }
}
@end
