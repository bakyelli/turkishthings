//
//  FSVenuePhoto.m
//  TurkishThings
//
//  Created by Basar Akyelli on 12/20/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "FSVenuePhoto.h"

@implementation FSVenuePhoto

-(id)initWithPhotoDictionary:(NSDictionary *)dict withThumbnailSize:(NSInteger)size withBannerWidth:(NSInteger)bannerWidth withBannerHeight:(NSInteger)bannerHeight
{
    self = [super init];
    
    if(self)
    {
        _photoID = dict[@"id"];
        _createdAt = [NSDate dateWithTimeIntervalSince1970:[dict[@"createdAt"] integerValue]];
        _height = dict[@"height"];
        _width = dict[@"width"];
        _prefix = dict[@"prefix"];
        _suffix = dict[@"suffix"];
        _thumbnailURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%dx%d%@", _prefix,size,size,_suffix]];
        _bannerURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%dx%d%@", _prefix,bannerWidth,bannerHeight,_suffix]];
        _fullsizeURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@x%@%@", _prefix,_width,_height,_suffix]];
        
    }
    return self;
}

+(NSArray *)convertToPhotos:(NSArray *)photos withThumbnailSize:(NSInteger)size withBannerWidth:(NSInteger)bannerWidth withBannerHeight:(NSInteger)bannerHeight
{
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:[photos count]];
    
    for(NSDictionary *photo in photos)
    {
        FSVenuePhoto *venuePhoto = [[FSVenuePhoto alloc]initWithPhotoDictionary:photo withThumbnailSize:size withBannerWidth:bannerWidth withBannerHeight:bannerHeight];
        [objects addObject:venuePhoto];
        
    }
    return objects;
}


@end
