//
//  StoredVenueImages.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/30/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "StoredVenueImage.h"

@implementation StoredVenueImage
-(id) init{
    self = [super init];
    
    if(self)
    {
        self.uniqueID = [[NSProcessInfo processInfo] globallyUniqueString];
    }
    
    return self;
}

-(id) initWithParseObjectForThumbnails:(PFObject *)object
{
    self = [super init];
    if (self)
    {
        self.createdByUserName = object[@"createdByUserName"];
        self.createdByUserReviewerName = object[@"createdByUserReviewerName"][0];
        self.uniqueID = object[@"uniqueID"];
        self.storedVenueUniqueID = object[@"storedVenueUniqueID"];
   //     self.reviewImage = object[@"reviewImage"];
        self.reviewImageThumbnail = object[@"reviewImageThumbnail"];
    }
    return self;
    
}
-(id) initWithParseObjectForSingleFullSizedImage:(PFObject *)object
{
    self = [super init];
    if (self)
    {
        self.reviewImage = object[@"reviewImage"];
    }
    return self;
    
}
@end
