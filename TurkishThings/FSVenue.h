//
//  FSVenue.h
//  APIsAndBlocks
//
//  Created by Basar Akyelli on 10/24/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSVenue : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *venueID;
@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longtitude;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *foursquareID;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *zipCode;
@property (nonatomic, strong) NSNumber *checkinCount;
@property (nonatomic, strong) NSNumber *tipCount; 
@property (nonatomic, strong) NSString *webURL;
@property (nonatomic, strong) NSString *categoryImageURL;
+(NSArray *) convertToVenues:(NSArray *)venues;

@end
