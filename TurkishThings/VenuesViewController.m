//
//  VenuesViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "VenuesViewController.h"
#import "AddVenueViewController.h"
#import "DataStore.h"
#import <Parse/Parse.h>
#import "StoredVenueDetailsViewController.h"
#import <FontAwesomeKit.h>
#import <SVProgressHUD.h>
#import "VenueFilter.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "MenuViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <UIAlertView+Blocks.h>
#import "ImageLibrary.h"
#import "LocationSettingsNavigationController.h"
#import "RootNavigationController.h"
#import <Reachability.h>
#import "UIViewController+Methods.h"
#import "AppDelegate.h"

@interface VenuesViewController ()
@property (nonatomic) StoredVenue *venueSelected;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (assign) BOOL mapIsFullScreen;
@property (assign) CGRect mapOriginalRect;
@property (assign) CGRect tableViewOriginalRect;
@property (strong,nonatomic) UIButton *shrinkButton;
@property (strong,nonatomic) UIButton *centerButton;
@property BOOL isPopulatingVenues;

- (IBAction)btnCenterPressed:(id)sender;

@end

@implementation VenuesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self logOffIfUserIsNil];
    [self connectMeToMyMenu];
    
    self.mapOriginalRect = self.mapView.frame;
    self.tableViewOriginalRect = self.tableView.frame;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;    
    
    self.mapView.delegate = self;
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.locationManager.distanceFilter = 100;
    
    [self locate];
    
    [[self navigationController] setNavigationBarHidden:NO];
    self.navigationItem.hidesBackButton = YES;
    
    FAKFontAwesome *icon = [FAKFontAwesome barsIconWithSize:20];
    [icon addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor]];
    UIImage *iconImage = [icon imageWithSize:CGSizeMake(20, 20)];
    
    UIBarButtonItem *menuBarButtonItem = [[UIBarButtonItem alloc]initWithImage:iconImage style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonPressed:)];
    
    [self.navigationItem setLeftBarButtonItem:menuBarButtonItem];
    
    [self fixBackButton];
    
    self.useCurrentLocation = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"Location Settings Changed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"Venues Updated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:kReachabilityChangedNotification object:nil];


}
-(void)logOffIfUserIsNil
{

    if(![PFUser currentUser]){
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];

    RootNavigationController *rnc = [storyboard instantiateViewControllerWithIdentifier:@"contentController"];
    [PFUser logOut];
    [[PFUser currentUser] removeObjectForKey:@"userReviewerName"];
    [[PFUser currentUser] removeObjectForKey:@"profilePicture"];
    [[PFUser currentUser] setObject:nil forKey:@"userReviewerName"];
    [[PFUser currentUser] setObject:nil forKey:@"profilePicture"];
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
    [self.mm_drawerController setCenterViewController:rnc withCloseAnimation:(YES) completion:^(BOOL finished) {
    }];
    }
    AppDelegate *appDel = [[UIApplication sharedApplication] delegate];
    
    if(appDel.executionType == AppExecutionTypeDev)
    {
        [self setTitle:@"DEV DEV DEV"];
    }
    

}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.tableView.contentInset = UIEdgeInsetsMake(self.mapView.frame.size.height-40, 0, 0, 0);
    
    if(![self hasFourInchDisplay])
    {
        [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height-88)];
    }
    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y < self.mapView.frame.size.height*-1 ) {
        [self mapFullScreen];
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, self.mapView.frame.size.height*-1)];
        
    }

}

-(void)receivedNotification:(NSNotification *)notification
{
    if([[notification name] isEqualToString:@"Location Settings Changed"])
    {
        if(!self.useCurrentLocation)
        {
            [self refreshVenues];
            [self centerMapView];
        }
        else
        {
            [self.locationManager startUpdatingLocation];
        }
    }
    else if([[notification name] isEqualToString:@"Venues Updated"])
    {
        [self refreshVenues];
        [self centerMapView];
    }
    else if([[notification name] isEqualToString:UIApplicationDidBecomeActiveNotification])
    {
        if(self.useCurrentLocation)
        {
            [self locate];
        }

    }
    else if([[notification name] isEqualToString:kReachabilityChangedNotification])
    {
        Reachability *reach = (Reachability *)notification.object;
        if(reach.isReachable)
        {
            if(self.useCurrentLocation)
            {
                [self locate];
            }
        }
      
    }
}

-(void)mapFullScreen
{
    [UIView animateWithDuration:0.2f animations:^{
    
        [self.mapView setFrame:CGRectMake(self.mapView.frame.origin.x,
                                          self.mapView.frame.origin.y,
                                          self.mapView.frame.size.width,
                                          self.view.frame.size.height)];
        
        [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x,
                                            self.tableView.frame.origin.y+self.view.frame.size.height,
                                            self.tableView.frame.size.width,
                                            self.tableView.frame.size.height)];
        self.mapIsFullScreen = YES;
        
        
        self.shrinkButton = [[UIButton alloc]initWithFrame:CGRectMake(5,self.view.center.y,30,30)];
        [self.shrinkButton setImage:[ImageLibrary ShrinkImageWithSize:30 withColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [self.shrinkButton setBackgroundColor:[UIColor darkGrayColor]];
        [self.shrinkButton addTarget:self action:@selector(mapShrink:) forControlEvents:UIControlEventTouchUpInside];
        [self.mapView addSubview:self.shrinkButton];
        
        self.centerButton = [[UIButton alloc]initWithFrame:CGRectMake(5,self.view.center.y-40,30,30)];
        [self.centerButton setImage:[ImageLibrary NavigateImageWithSize:30 withColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [self.centerButton setBackgroundColor:[UIColor darkGrayColor]];
        [self.centerButton addTarget:self action:@selector(btnCenterPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.mapView addSubview:self.centerButton];

    }];
    
}
-(void)mapShrink:(id)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        [self.mapView setFrame:self.mapOriginalRect];
        [self.tableView setFrame:self.tableViewOriginalRect];
        self.mapIsFullScreen = NO;
    }];
    [self.mapView deselectAnnotation:self.mapView.selectedAnnotations[0] animated:NO];
    [self.shrinkButton removeFromSuperview];
    [self.centerButton removeFromSuperview];
}

-(void)locate
{
    [self.locationManager startUpdatingLocation];
    [self showActivityIndicator];

}

-(void)connectMeToMyMenu
{
    MenuViewController *mvc = (MenuViewController *)self.mm_drawerController.leftDrawerViewController;
    mvc.venuesViewController = self;
}
-(void)categoryPressed:(VenueCategory *)selectedCategory
{
    VenueFilter *filter = [VenueFilter new];
    filter.filterWithVenueCategory= YES;
    filter.venueCategory = selectedCategory;
    filter.filterWithLocation = YES;
    filter.location = self.currentLocation;
    
    [self populateVenuesWithFilter:filter];
}
-(void)refreshVenues
{
    VenueFilter *filter = [VenueFilter new];
    filter.filterWithLocation = YES;
    filter.location = self.currentLocation;
    
    [self populateVenuesWithFilter:filter];
}
- (void)refresh:(UIRefreshControl *)refreshControl {
    [self refreshVenues];
    [refreshControl endRefreshing];
}

-(void) populateVenuesWithFilter:(VenueFilter *)filter
{
    [NSTimer scheduledTimerWithTimeInterval:10.0
                                     target:self
                                   selector:@selector(timeoutcheck:)
                                   userInfo:nil
                                    repeats:NO];
    self.isPopulatingVenues = YES;
    [self showActivityIndicator];
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    [[DataStore SharedStore] getStoredVenuesWithVenueFilter:filter withCompletion:^(NSMutableArray *storedvenues){
        self.venues = storedvenues;
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];

        for(StoredVenue *venue in storedvenues)
        {
            [self.mapView addAnnotation:venue];
        }
        [self dismissActivityIndicator];
        self.isPopulatingVenues = NO;
    }];
}
-(void)timeoutcheck:(id)sender
{
    if(self.isPopulatingVenues)
    {
        self.isPopulatingVenues = NO;
        [[DataStore SharedStore] showErrorProgressWithText:@"Error connecting to servers. Please try again later."];
    }
}


- (BOOL)hasFourInchDisplay {
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0);
}


#pragma mark - UITableViewDelegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        return 18;
    }
    else if(indexPath.section == 1)
    {
        return 57;
    }
    else
    {
        return 44;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 1;
    }
    else if(section == 1)
    {
    return [self.venues count];
    }
    else if(section ==2)
    {
        if ([self.venues count] == 0)
        {
            return 2;
        }
        else
        {
            return 0;
        }
    }
    
    return 0;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *venueCellIdentifier = @"cell";
    NSString *headerCellIdentifier = @"headerCell";
    NSString *noVenueCellIdentifier = @"noVenuesCell";
    NSString *locationSettingsCellIdentifier = @"locationSettingsCell";
    
    if(indexPath.section == 0)
    {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:headerCellIdentifier];
        UILabel *headerLabel = (UILabel *)[cell.contentView viewWithTag:1];
        headerLabel.text = @"Venues near your location";
        
        return cell;

    }
    else if (indexPath.section == 1)
    {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:venueCellIdentifier];
        
        StoredVenue *venue = self.venues[indexPath.row];
        
        UILabel *venueTitleLabel = (UILabel *)[cell.contentView viewWithTag:6];
        venueTitleLabel.text = venue.title;
        
        int venueRating = [venue calculateRating];
        EDStarRating *starRatingImage = (EDStarRating *)[cell.contentView viewWithTag:99];
        [self prepareRating:starRatingImage withRating:venueRating];
        
//        UILabel *reviewCountLabel = (UILabel *)[cell.contentView viewWithTag:1];
//       reviewCountLabel.text = [NSString stringWithFormat:@"%@ reviews",venue.ratingCount];
        
        UILabel *distanceLabel = (UILabel *)[cell.contentView viewWithTag:3];
        double distance = [self calculateDistanceBetweenSelfAndVenue:venue];
        distanceLabel.text = [NSString stringWithFormat:@"%.2f km",distance];
        
        
        UIImageView *categoryImageView = (UIImageView *)[cell.contentView viewWithTag:5];
        categoryImageView.image = [VenueCategory categoryIconForCategoryString:venue.category withSize:25 withColor:[UIColor darkGrayColor]];
        
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

        
        //UIImageView *commentsImageView = (UIImageView *)[cell.contentView viewWithTag:7];
        //commentsImageView.image = [ImageLibrary CommentsImageWithSize:15 withColor:[UIColor blackColor]];

        
        return cell;
    }
    else if(indexPath.section == 2)
    {
        if(indexPath.row == 0)
        {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:noVenueCellIdentifier];
        return cell;
        }
        else if(indexPath.row == 1)
        {
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:locationSettingsCellIdentifier];
            return cell;

        }
    }
    return nil;
}


-(double)calculateDistanceBetweenSelfAndVenue:(StoredVenue *)venue
{
    CLLocation *locVenue = [[CLLocation alloc]initWithLatitude:venue.coordinate.latitude longitude:venue.coordinate.longitude];
    CLLocation *userLocation = self.currentLocation;
    
    CLLocationDistance distance = [userLocation distanceFromLocation:locVenue];
    
    return distance/1000;
    

}

- (void)prepareRating:(EDStarRating *)_starRatingImage withRating:(int)venueRating
{
    //_starRatingImage.starImage = [UIImage imageNamed:@"star.png"];
    _starRatingImage.starImage = [ImageLibrary StarImageWithSize:15 withColor:[UIColor whiteColor]];

   // _starRatingImage.starHighlightedImage = [UIImage imageNamed:@"starhighlighted.png"];
    _starRatingImage.starHighlightedImage = [ImageLibrary StarImageWithSize:15 withColor:[UIColor redColor]];

    _starRatingImage.maxRating = 5;;
    _starRatingImage.horizontalMargin = 1;
    _starRatingImage.editable=NO;
    _starRatingImage.rating= venueRating;
    _starRatingImage.displayMode=EDStarRatingDisplayAccurate;

}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if([annotation isKindOfClass:[StoredVenue class]])
    {
    
        StoredVenue *venue = (StoredVenue *)annotation;
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"StoredVenueAnnotation"];
        annotationView = venue.annotationView;
        
        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoLight];
        return annotationView;
    }
    else
    {
        return nil;
    }
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    self.venueSelected = (StoredVenue *)view.annotation;
    [self performSegueWithIdentifier:@"storedVenueDetails" sender:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)fixBackButton
{
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: @"" style: UIBarButtonItemStylePlain target: nil action: nil];
    [[self navigationItem] setBackBarButtonItem: newBackButton];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString: @"addVenue"] || [segue.identifier isEqualToString:@"addFirstVenue"])
    {
        if(self.currentLocation != nil)
        {
            AddVenueViewController *avvc = (AddVenueViewController *)segue.destinationViewController;
            avvc.currentLocation = self.currentLocation;
            avvc.vvc = self; 
        }
        else
        {
            UIAlertView *errorView = [[UIAlertView alloc]initWithTitle:@"Location Error"
                                                               message:@"We need your location!"
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil, nil];
            [errorView show];
        }
    }
    else if([segue.identifier isEqualToString:@"storedVenueDetails"])
    {
        StoredVenueDetailsViewController *svdvc = segue.destinationViewController;

        if((UITableViewCell *)sender != nil)
        {
            self.venueSelected = self.venues[self.tableView.indexPathForSelectedRow.row];
        }
        svdvc.venue = self.venueSelected;
        
    }
    else if([segue.identifier isEqualToString:@"locationSettings"])
    {
        LocationSettingsNavigationController *lsnc = (LocationSettingsNavigationController *) segue.destinationViewController;
        lsnc.vvc = self;
        lsnc.useCurrentLocation = self.useCurrentLocation;
        lsnc.currentLocation = self.currentLocation;
        
    }
}

#pragma mark - CLLocationManager delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    if(self.useCurrentLocation){
        [self dismissActivityIndicator];
        NSLog(@"Found location");
        self.currentLocation = manager.location;
        
        VenueFilter *filter = [VenueFilter new];
        filter.filterWithLocation = YES;
        filter.location = self.currentLocation;
        [self populateVenuesWithFilter:filter];
        [self centerMapView];
    }

}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    
    if(self.useCurrentLocation)
    {
        [self dismissActivityIndicator];
        NSLog(@"Error updating location");
        [UIAlertView showWithTitle:@"Problem"
                           message:@"We are having issues locating you! We need your location to list nearby venues."
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:@[@"Try again"]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              if(buttonIndex != alertView.cancelButtonIndex)
                              {
                                  [self locate];
                              }
                          }];
    }
}
-(void) centerMapView
{
    CLLocationCoordinate2D loc = self.currentLocation.coordinate;
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 3000, 3000);
    [self.mapView setRegion:region animated:YES];
}
- (void)menuButtonPressed:(id)sender {
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

- (IBAction)btnCenterPressed:(id)sender {
    [self centerMapView];
}
@end
