//
//  StoredVenueReview.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/8/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "StoredVenueReview.h"

@implementation StoredVenueReview

-(id) init{
    self = [super init];
    
    if(self)
    {
        self.uniqueID = [[NSProcessInfo processInfo] globallyUniqueString];
    }
    
    return self;
}
-(id) initWithParseObject:(PFObject *)object
{
    self = [super init];
    if (self)
    {
        self.createdByUserName = object[@"createdByUserName"];
        self.createdByUserReviewerName = object[@"createdByUserReviewerName"][0];
        self.review = object[@"review"];
        self.uniqueID = object[@"uniqueID"];
        self.storedVenueUniqueID = object[@"storedVenueUniqueID"];
        self.rating = [NSNumber numberWithInt:[object[@"rating"] integerValue]];
        self.createdAt = object.createdAt;
        self.parseObject = object; 
    }
    return self;
    
}

@end
