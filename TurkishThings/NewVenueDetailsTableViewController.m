//
//  NewVenueDetailsTableViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "NewVenueDetailsTableViewController.h"
#import "VenueCategory.h"
#import "DataStore.h"
#import <CoreLocation/CoreLocation.h>
#import "StoredVenue.h"
#import <Parse/Parse.h>
#import "StoredVenueReview.h"
#import <FAKFontAwesome.h>
#import <FAKFoundationIcons.h>
#import <SVProgressHUD.h>
#import "UIImage+Resize.h"
#import "StoredVenueImage.h"
#import <UIAlertView+Blocks.h>
#import "ImageLibrary.h"
@interface NewVenueDetailsTableViewController ()

@property (nonatomic, strong) UIImage *reviewImage;
@property (nonatomic, strong) UIImage *reviewImageThumbnail;
@property (weak, nonatomic) IBOutlet UILabel *reviewLengthLabel;

@end

@implementation NewVenueDetailsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)prepareRating
{
    _starRatingImage.starImage = [ImageLibrary StarImageWithSize:20 withColor:[UIColor lightGrayColor]];
    _starRatingImage.starHighlightedImage = [ImageLibrary StarImageWithSize:20 withColor:[UIColor redColor]];
    _starRatingImage.maxRating = 5.0;
    _starRatingImage.delegate = self;
    _starRatingImage.horizontalMargin = 1;
    _starRatingImage.editable = YES;
    _starRatingImage.rating= 0;
    _starRatingImage.displayMode=EDStarRatingDisplayAccurate;
    _starRatingImage.returnBlock = ^(float rating )
    {
        
        NSLog(@"Reading the starrating rating: %.0f", _starRatingImage.rating);
        NSLog(@"ReturnBlock: Star rating changed to %.1f", rating);
    };
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];

    
    if(self.isExistingVenue)
    {
        self.tbVenueName.text = self.existingVenue.title;
        self.lblCategory.text = self.existingVenue.category;
        self.categoryImageView.image = [VenueCategory categoryIconForCategoryString:self.existingVenue.category withSize:20 withColor:[UIColor blackColor]];
    }
    else
    {
        self.tbVenueName.text = self.venue.name;

    }

    
    [self prepareRating];
    self.tbVenueName.delegate = self;
    self.tbVenueReview.delegate = self;
    
    self.tbVenueReview.layer.borderWidth = 2.0f;
    self.tbVenueReview.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self showVenueOnMap];
    [self setUpCameraButton];
}

-(void)setUpCameraButton
{
    [self.btnCamera setHidden:YES];
    FAKFontAwesome *icon = [FAKFontAwesome cameraIconWithSize:25];
    UIImage *iconImage = [icon imageWithSize:CGSizeMake(25, 25)];
    [self.btnCamera setBackgroundImage:iconImage forState:UIControlStateNormal];
    [self.btnCamera setTitle:@"" forState:UIControlStateNormal];
    
    [self.btnDeletePicture setHidden:YES];
    FAKFoundationIcons *iconEraser = [FAKFoundationIcons xIconWithSize:25];
    [iconEraser addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]];
    UIImage *iconImageEraser = [iconEraser imageWithSize:CGSizeMake(25, 25)];
    [self.btnDeletePicture setBackgroundImage:iconImageEraser forState:UIControlStateNormal];
    [self.btnDeletePicture setTitle:@"" forState:UIControlStateNormal];
}
- (void) showVenueOnMap
{
    CLLocationCoordinate2D coord;
    
    if(self.isExistingVenue)
    {
        coord = self.existingVenue.coordinate;
    }
    else
    {
        coord = CLLocationCoordinate2DMake([self.venue.latitude doubleValue], [self.venue.longtitude doubleValue]);
    }
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc]init];
    annotation.coordinate = coord;
    
    [self.mapView addAnnotation:annotation];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 2000, 2000);

    [self.mapView setRegion:region animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextBoxDelegate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
}

#pragma mark - UITextViewDelegate methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    self.reviewLengthLabel.text = [NSString localizedStringWithFormat:@"%lu/%d",(unsigned long)textView.text.length, 250];
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return textView.text.length + (text.length - range.length) <= 250;
}

#pragma mark - IBAction

- (void)btnSubmitPressed
{
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    if([self.tbVenueReview.text length] < 30)
    {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter at least 30 characters for your venue review!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if(self.starRatingImage.rating == 0)
    {
        [UIAlertView showWithTitle:nil message:@"You need to give a rating by tapping on the stars!" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
    }
    else
    {
        if(self.isExistingVenue) //adding a review to an existing venue.
        {
            StoredVenueReview *newVenueReview = [[StoredVenueReview alloc]init];
            newVenueReview.rating = [NSNumber numberWithFloat:self.starRatingImage.rating];
            newVenueReview.createdByUserName = [PFUser currentUser].username;
            newVenueReview.createdByUserReviewerName = [PFUser currentUser][@"userReviewerName"];
            newVenueReview.review = self.tbVenueReview.text;
            newVenueReview.storedVenueUniqueID = self.existingVenue.uniqueID;
            
            StoredVenueImage *venueImage;
            
            if(self.reviewImage != nil)
            {
                venueImage = [[StoredVenueImage alloc]init];
                
                NSData *imageData = UIImagePNGRepresentation(self.reviewImage);
                PFFile *imageFile = [PFFile fileWithData:imageData];
                
                NSData *imageThumbnailData = UIImagePNGRepresentation(self.reviewImageThumbnail);
                PFFile *imageThumbnailFile = [PFFile fileWithData:imageThumbnailData];
                
                venueImage.reviewImage = imageFile;
                venueImage.storedVenueUniqueID = newVenueReview.storedVenueUniqueID;
                venueImage.createdByUserName = newVenueReview.createdByUserName;
                venueImage.createdByUserReviewerName = newVenueReview.createdByUserReviewerName;
                venueImage.reviewImageThumbnail =imageThumbnailFile;
                
            }
            
            [[DataStore SharedStore] showProgressWithText:@"Creating review..."];
            [[DataStore SharedStore] saveStoredVenueReviewToCloud:newVenueReview completionBlock:^(BOOL result) {
                if(result)
                {
                    
                //    [self.existingVenue calculateRatingAndUpdateWithReview:newVenueReview];
                    
                    if(venueImage != nil)
                    {
                        [[DataStore SharedStore] saveStoredVenueImageToCloud:venueImage completionBlock:^(BOOL result) {
                            [[DataStore SharedStore] showSuccessProgressWithText:@"Success!"];
                            [self popControllersNumber:2];
                        }];
                    }
                    else
                    {
                        [[DataStore SharedStore] showSuccessProgressWithText:@"Success!"];
                        [self sendNotificaiton];
                        [self popControllersNumber:2];
                    }
                }
            }];
        }
        else{ //this is a new venue
            if([self.lblCategory.text isEqual: @""])
            {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"You need to pick a category!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                
            }
            else
            {
                StoredVenue *newVenue = [[StoredVenue alloc]init];
                newVenue.foursquareVenue = self.venue;
                newVenue.title = self.tbVenueName.text;
                newVenue.ratingCount = @1;
                newVenue.ratingTotal = [NSNumber numberWithFloat:self.starRatingImage.rating];
                newVenue.coordinate = CLLocationCoordinate2DMake([self.venue.latitude doubleValue], [self.venue.longtitude doubleValue]);
                newVenue.category = self.lblCategory.text;
                newVenue.city = self.venue.city;
                newVenue.country = self.venue.country;
                newVenue.isFromFoursquare = YES;
                newVenue.createdByUserName = [PFUser currentUser].username;
                newVenue.createdByUserReviewerName = [PFUser currentUser][@"userReviewerName"];
                newVenue.foursquareVenueID = self.venue.foursquareID;
                newVenue.address = self.venue.address;
                newVenue.zipCode = self.venue.zipCode;
                newVenue.webURL = self.venue.webURL;
                
                StoredVenueReview *newVenueReview = [[StoredVenueReview alloc]init];
                newVenueReview.rating = [NSNumber numberWithFloat:self.starRatingImage.rating];
                newVenueReview.createdByUserName = [PFUser currentUser].username;
                newVenueReview.createdByUserReviewerName = [PFUser currentUser][@"userReviewerName"];
                newVenueReview.review = self.tbVenueReview.text;
                newVenueReview.storedVenueUniqueID = newVenue.uniqueID;
                
                StoredVenueImage *venueImage;
                
                if(self.reviewImage != nil)
                {
                    venueImage = [[StoredVenueImage alloc]init];
                    
                    NSData *imageData = UIImagePNGRepresentation(self.reviewImage);
                    PFFile *imageFile = [PFFile fileWithData:imageData];
                    
                    NSData *imageThumbnailData = UIImagePNGRepresentation(self.reviewImageThumbnail);
                    PFFile *imageThumbnailFile = [PFFile fileWithData:imageThumbnailData];
                    
                    
                    venueImage.reviewImage = imageFile;
                    venueImage.reviewImageThumbnail =imageThumbnailFile;
                    venueImage.storedVenueUniqueID = newVenueReview.storedVenueUniqueID;
                    venueImage.createdByUserName = newVenueReview.createdByUserName;
                    venueImage.createdByUserReviewerName = newVenueReview.createdByUserReviewerName;
                    
                    
                    
                }
                
                [[DataStore SharedStore] showProgressWithText:@"Creating venue..."];
                
                [[DataStore SharedStore] saveStoredVenueToCloud:newVenue completionBlock:^(BOOL result) {
                    if(result)
                    {
                        NSLog(@"Successfuly created venue");
                        
                        [[DataStore SharedStore] saveStoredVenueReviewToCloud:newVenueReview completionBlock:^(BOOL result) {
                            if(result)
                            {
                                
                                if(venueImage != nil)
                                {
                                    [[DataStore SharedStore] saveStoredVenueImageToCloud:venueImage completionBlock:^(BOOL result) {
                                        [self sendNotificaiton];

                                        [[DataStore SharedStore] showSuccessProgressWithText:@"Success!"];
                                        [self popControllersNumber:2];
                                    }];
                                }
                                else
                                {
                                    [self sendNotificaiton];

                                    [[DataStore SharedStore] showSuccessProgressWithText:@"Success!"];
                                    [self popControllersNumber:2];
                                }
                                
                                
                            }
                        }];
                        
                    }
                    else
                    {
                        NSLog(@"Error occured inserting the object");
                    }
                }];
                
            }
            
        }
    }
}
- (void)sendNotificaiton
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Venues Updated" object:self];
}
- (IBAction)btnCancelPressed:(id)sender {
    [self popControllersNumber:2];
}

- (IBAction)btnAddPicturePressed:(id)sender {
    
    
    UIActionSheet *photoActionSheet = [[UIActionSheet alloc]initWithTitle:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                      destructiveButtonTitle:nil
                                                           otherButtonTitles:nil, nil];
    
    
    [photoActionSheet setTag:1];
    
    [photoActionSheet addButtonWithTitle:@"Camera"];
    [photoActionSheet addButtonWithTitle:@"Choose from library"];
    [photoActionSheet addButtonWithTitle:@"Cancel"];
    photoActionSheet.cancelButtonIndex = [photoActionSheet numberOfButtons]-1;
    
    [photoActionSheet showInView:[UIApplication sharedApplication].keyWindow];
}



- (void) popControllersNumber:(int)number
{
    if (number <= 1)
        [[self navigationController] popViewControllerAnimated:YES];
    else
    {
        NSArray* controller = [[self navigationController] viewControllers];
        int requiredIndex = [controller count] - number - 1;
        if (requiredIndex < 0) requiredIndex = 0;
        UIViewController* requireController = [[[self navigationController] viewControllers] objectAtIndex:requiredIndex];
        [[self navigationController] popToViewController:requireController animated:YES];
    }
}
- (IBAction)btnDeletePicturePressed:(id)sender {
    self.reviewImage = nil;
    [self.btnCamera setHidden:YES];
    [self.btnDeletePicture setHidden:YES];
    [[DataStore SharedStore]showSuccessProgressWithText:@"Picture Deleted!"];
}
- (IBAction)btnPickCategoryPressed:(id)sender {
    
    if(!self.isExistingVenue){
    UIActionSheet *categoryActionSheet = [[UIActionSheet alloc]initWithTitle:@"Pick Venue Category"
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                      destructiveButtonTitle:nil
                                                           otherButtonTitles:nil, nil];
    
    
    [categoryActionSheet setTag:0];
    NSMutableArray *categoriesArray = [DataStore SharedStore].venueCategories;
    
    for(VenueCategory *category in categoriesArray)
    {
    [categoryActionSheet addButtonWithTitle:category.categoryName];
    
    }
    [categoryActionSheet addButtonWithTitle:@"Cancel"];
    categoryActionSheet.cancelButtonIndex = [categoryActionSheet numberOfButtons]-1;
    
    [categoryActionSheet showInView:[UIApplication sharedApplication].keyWindow];
    }
    else
    {
        [UIAlertView showWithTitle:nil message:@"You cannot assign a category to an existing venue." cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
        }];
    }
    
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet.tag == 0){
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            self.lblCategory.text = [actionSheet buttonTitleAtIndex:buttonIndex];
            self.categoryImageView.image = [VenueCategory categoryIconForCategoryString:[actionSheet buttonTitleAtIndex:buttonIndex] withSize:20 withColor:[UIColor blackColor]];
        }
    }
    else if(actionSheet.tag == 1)
    {
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            if(buttonIndex == 0)//Camera
            {
                UIImagePickerController* picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    [self presentViewController:picker animated:YES completion:nil];
                }
                else
                {
                    UIAlertView *altnot=[[UIAlertView alloc]initWithTitle:@"Oops!" message:@"Camera not available" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [altnot show];
                }
                
            }
            else if(buttonIndex == 1)//Library
            {
                UIImagePickerController* picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
                [self presentViewController:picker animated:YES completion:nil];
            }
            
        }
        
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.btnCamera setHidden:NO];
    [self.btnDeletePicture setHidden:NO];
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *pickedImage =[info objectForKey:UIImagePickerControllerOriginalImage];
    
    self.reviewImage = [pickedImage resizedImageToFitInSize:CGSizeMake(350,350) scaleIfSmaller:YES];
    self.reviewImageThumbnail = [pickedImage resizedImageToFitInSize:CGSizeMake(100,100) scaleIfSmaller:YES];
    
    
}

- (IBAction)btnCameraBtnPressed:(id)sender {
//    TGRImageViewController *viewController = [[TGRImageViewController alloc] initWithImage:self.reviewImage];
//    // Don't forget to set ourselves as the transition delegate
//    [self presentViewController:viewController animated:YES completion:nil];
}






@end
