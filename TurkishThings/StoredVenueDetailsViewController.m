//
//  StoredVenueDetailsViewController.m
//  TurkishThings
//
//  Created by Basar Akyelli on 11/8/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import "StoredVenueDetailsViewController.h"
#import "DataStore.h"
#import <SVProgressHUD.h>
#import <UIActionSheet+Blocks.h>
#import <UIAlertView+Blocks.h>
#import "ImageLibrary.h"
#import "AddVenueDetailsViewController.h"
#import "VenueImagesCollectionViewController.h"
#import <FISWebViewPreloader.h>
#import "WebViewViewController.h"
#import <MMDrawerController/UIViewController+MMDrawerController.h>
#import "ReportVenue.h"
#import "UIViewController+Methods.h"
#import <Foursquare-API-v2/Foursquare2.h>
#import "FSVenuePhoto.h"
#import <UIImageView+AFNetworking.h>
#import <FSImageViewer/FSBasicImage.h>
#import <FSImageViewer/FSBasicImageSource.h>
#import <FSImageViewer/FSImageViewerViewController.h>


@interface StoredVenueDetailsViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btnReportVenue;
@property (weak, nonatomic) IBOutlet UILabel *reviewLengthLabel;
@property (strong, nonatomic) FISWebViewPreloader *webViewPreloader;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong,nonatomic) NSMutableArray *imagesArray;
@property (strong,nonatomic) NSMutableArray *imageArrayForSource;
@property (strong,nonatomic) FSBasicImageSource *basicSource;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *venueInfoView;

@end

@implementation StoredVenueDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareRating];
    [self loadFoursquareImages];
    
    self.lblVenueName.text = self.venue.title;
        
    self.lblAddressInfo.text = [NSString stringWithFormat:(@"%@, %@, %@ %@"),
                                self.venue.address, self.venue.city, self.venue.country,
                                self.venue.zipCode];
    
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
        
    [self populateReviews];
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    
    
    UIColor * color = [UIColor colorWithRed:255/255.0f green:145/255.0f blue:20/255.0f alpha:1.0f];
    [self.btnReportVenue setBackgroundImage:[ImageLibrary AlertImageWithSize:18 withColor:color] forState:UIControlStateNormal];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(containerTapped:)];
    [singleTap setNumberOfTapsRequired:1];
    
    [self.venueInfoView addGestureRecognizer:singleTap];
    
}

-(void)containerTapped:(id)sender
{
    [self imagesButtonPressed:nil];
}
-(void)makeNavBarTransparent
{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

}
- (IBAction)reportVenueBtnPressed:(id)sender {
    [UIActionSheet showInView:self.view
                    withTitle:nil
            cancelButtonTitle:@"Cancel"
       destructiveButtonTitle:nil
            otherButtonTitles:@[@"Permanently Closed", @"Not Suitable for Turkish Things"]
                     tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
                         
                         ReportVenue *report = [ReportVenue new];
                         report.storedVenueUniqueID = self.venue.uniqueID;
                         report.venueName = self.venue.title;
                         report.createdByUserName = [PFUser currentUser].username;
                         report.createdByUserReviewerName = [PFUser currentUser][@"userReviewerName"];
                         report.reportVenueReasonType = [actionSheet buttonTitleAtIndex:buttonIndex];
                         [[DataStore SharedStore]reportVenueWithReport:report];
                         
                         
                         if(actionSheet.cancelButtonIndex != buttonIndex){
                         [UIAlertView showWithTitle:@"Thanks!" message:@"We'll take a look at it" cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];
                         }
                         
                     }];
}
- (void)prepareToolbar
{
    UIToolbar *toolBar;
    CGRect rect2 = CGRectMake(0,  [[UIScreen mainScreen] bounds].size.height - 44 , self.view.frame.size.width , 0);
    toolBar = [[UIToolbar alloc]initWithFrame:rect2];
    
    [toolBar setTranslucent:NO];
    [toolBar setBarTintColor:[UIColor darkGrayColor]];
    
    [toolBar sizeToFit];
    
    
    UIBarButtonItem *mapsButton = [[UIBarButtonItem alloc]initWithImage:[ImageLibrary MapsImageWithSize:25 withColor:[UIColor blackColor]] style:UIBarButtonItemStylePlain target:self action:@selector(actionButtonPressed:)];
    mapsButton.width = 50;
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil ];
    
    
    
    UIBarButtonItem *imagesButton = [[UIBarButtonItem alloc]initWithImage:[ImageLibrary ImagesImageWithSize:25 withColor:[UIColor blackColor]] style:UIBarButtonItemStylePlain target:self action:@selector(imagesButtonPressed:)];
    imagesButton.width = 50;
    
    UIBarButtonItem *foursquareButton = [[UIBarButtonItem alloc]initWithImage:[ImageLibrary FoursquareImageWithSize:25 withColor:[UIColor blackColor]] style:UIBarButtonItemStylePlain target:self action:@selector(foursquareButtonPressed:)];
    
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc]initWithImage:[ImageLibrary ShareImageWithSize:25 withColor:[UIColor blackColor]] style:UIBarButtonItemStylePlain target:self action:@selector(shareButtonPressed:)];
    
    if(![self.venue.webURL isEqual: @""] && self.venue.webURL)
    {
        UIBarButtonItem *webURLButton = [[UIBarButtonItem alloc]initWithImage:[ImageLibrary HyperlinkImageWithSize:25 withColor:[UIColor blackColor]] style:UIBarButtonItemStylePlain target:self action:@selector(webURLButtonPressed:)];
        webURLButton.width = 50;
        
        self.webViewPreloader = [FISWebViewPreloader new];
        [self.webViewPreloader setURLString:self.venue.webURL forKey:self.venue.title withCGRect:self.view.bounds];
        
        [toolBar setItems:@[mapsButton,flex,foursquareButton,flex,webURLButton,flex,shareButton,flex,imagesButton]];
        
    }
    else
    {
        [toolBar setItems:@[mapsButton,flex,foursquareButton,flex,shareButton,flex,imagesButton]];
        
    }
    
    [self.view addSubview:toolBar];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if(![self hasFourInchDisplay])
    {
        [self.tableView setFrame:CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height-88)];
    }
}

- (BOOL)hasFourInchDisplay {
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];

    [self makeNavBarTransparent];
    [self prepareToolbar];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}
-(void)shareButtonPressed:(id)sender
{
    NSString *texttoshare = [NSString stringWithFormat:@"I just checked out %@ on Turkish Things iOS app @turkthingsapp.",self.venue.title];
    NSArray *activityItems = @[texttoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}
-(void)foursquareButtonPressed:(id)sender
{
    [UIAlertView showWithTitle:nil message:@"You will leave Turkish Things to launch the Foursquare app." cancelButtonTitle:@"No" otherButtonTitles:@[@"Ok"] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1)
        {
            [[DataStore SharedStore]launchFoursquareWithVenueID:self.venue.foursquareVenueID];
        }
    }];
}
-(void)webURLButtonPressed:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    WebViewViewController *webViewVC = [storyboard instantiateViewControllerWithIdentifier:@"showWebView"];
    webViewVC.venuePreLoadedWebView =[self.webViewPreloader webViewForKey:self.venue.title];
    webViewVC.storedVenue = self.venue;
    
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: @"" style: UIBarButtonItemStylePlain target: nil action: nil];
    [[self navigationItem] setBackBarButtonItem: newBackButton];

    
    
    [self.navigationController pushViewController:webViewVC animated:YES];
}
-(void)imagesButtonPressed:(id)sender
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    VenueImagesCollectionViewController *collectionsVC = [storyboard instantiateViewControllerWithIdentifier:@"venueImages"];
    
    collectionsVC.venue = self.venue;
    collectionsVC.imagesArray = self.imagesArray;
    collectionsVC.imageArrayForSource = self.imageArrayForSource;
    collectionsVC.basicSource = self.basicSource;
    
    [self presentViewController:collectionsVC animated:NO completion:nil];
    
}

- (IBAction)mapsButtonPressed:(id)sender {
    [self actionButtonPressed:nil];
}
-(void)actionButtonPressed:(id)sender
{
    [UIActionSheet showInView:self.view
                    withTitle:@"Choose Maps Application"
            cancelButtonTitle:@"Cancel"
       destructiveButtonTitle:nil
            otherButtonTitles:@[@"Google Maps", @"Apple Maps"]
                     tapBlock:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
                         NSLog(@"Chose %@", [actionSheet buttonTitleAtIndex:buttonIndex]);
                         if([[actionSheet buttonTitleAtIndex:buttonIndex]  isEqual: @"Apple Maps"])
                         {
                             [self openVenueInAppleMaps];
                         }
                         else if([[actionSheet buttonTitleAtIndex:buttonIndex]  isEqual: @"Google Maps"])
                         {
                             [self openVenueInGoogleMaps];
                         }
                     }];
}
-(void)openVenueInGoogleMaps
{
    
    NSString *venueTitleFormatted = [self.venue.title stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?q=%@&center=%f,%f&zoom=14",venueTitleFormatted, self.venue.coordinate.latitude, self.venue.coordinate.longitude]
]];
    } else {
        [UIAlertView showWithTitle:@"Error" message:@"Can't locate Google Maps on your device." cancelButtonTitle:@"OK" otherButtonTitles:nil tapBlock:nil];

    }
}
-(void)openVenueInAppleMaps
{
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:self.venue.coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:self.venue.title];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }
}
-(void) populateReviews
{
    //NSString *message = [NSString stringWithFormat:@"Getting reviews for %@", self.venue.title];
    //[SVProgressHUD showWithStatus:message maskType:SVProgressHUDMaskTypeGradient];
    [self showActivityIndicator];
    
    [[DataStore SharedStore] getStoredVenueReviewsForStoredVenueID:self.venue.uniqueID withCompletion:^(NSMutableArray *storedVenueReviews) {
        self.reviews = storedVenueReviews;
        [self.tableView reloadData];
        [self dismissActivityIndicator];
    }];
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"AddVenueReview"])
    {
        AddVenueDetailsViewController *avdvc = segue.destinationViewController;
        avdvc.existingVenue = self.venue;
        avdvc.isExistingVenue = YES;
        UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: @"" style: UIBarButtonItemStylePlain target: nil action: nil];
        [newBackButton setTintColor:[UIColor whiteColor]];
        [[self navigationItem] setBackBarButtonItem: newBackButton];
        
    }

}

#pragma mark - UITableViewDelegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.reviews)
    {
        return 2;
    }
    else
    {
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 1;
    }
    else if(section == 1)
    {
    return [self.reviews count];
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 0)
    {
        return 18;
    }
    else if(indexPath.section == 1)
    {
    StoredVenueReview *review = self.reviews[indexPath.row];

    int reviewLen = review.review.length;
    
    if (reviewLen < 50)
    {
        return 50;
    }
    else if (reviewLen >= 50 && reviewLen < 75)
    {
        return 60;
    }
    else if(reviewLen >= 75 && reviewLen <100)
    {
        return 70;
    }
    else if(reviewLen >= 100 && reviewLen <= 150)
    {
        return 85;
    }
    else if(reviewLen >=150 && reviewLen <=200)
    {
        return 90;
    }
    else
        return 110;
        
    }
    return 110;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(indexPath.section == 0)
    {
        UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"headerCell"];
        return cell;

    }
    else if(indexPath.section == 1)
    {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;

    StoredVenueReview *review = self.reviews[indexPath.row];
    
    UILabel *reviewerLabel = (UILabel *)[cell.contentView viewWithTag:3];
    reviewerLabel.text = review.createdByUserReviewerName;
    
    UITextView *reviewView = (UITextView *)[cell.contentView viewWithTag:1];
    reviewView.text = review.review;

    int venueRating = [review.rating integerValue];
    EDStarRating *starRatingImage = (EDStarRating *)[cell.contentView viewWithTag:2];
    [self prepareRating:starRatingImage withRating:venueRating];
    
    UILabel *dateLabel = (UILabel *)[cell.contentView viewWithTag:4];
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = @"MM/dd/yy";
    dateLabel.text = [formatter stringFromDate:review.createdAt];
    
//    UIButton *trashButton = (UIButton *)[cell.contentView viewWithTag:80];
//    [trashButton setBackgroundImage:[ImageLibrary TrashImageWithSize:20 withColor:[UIColor blackColor]] forState:UIControlStateNormal];
//    [trashButton addTarget:self action:@selector(deleteVenueReview:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    }
    return nil;
}

#pragma mark - UICollectionView DataSource methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    UIImageView *venueImage = (UIImageView *)[cell viewWithTag:1];
    FSVenuePhoto *venuePhoto = self.imagesArray[indexPath.row];
    [venueImage setImageWithURL:venuePhoto.bannerURL];
    venueImage.alpha = 0.5;
    
    return cell;

}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self imagesButtonPressed:nil];
}

-(void)loadFoursquareImages
{
    [Foursquare2 venueGetPhotos:self.venue.foursquareVenueID limit:@50 offset:0 callback:^(BOOL success, id result) {
        [SVProgressHUD dismiss];
        if(success)
        {
            if ([result[@"response"][@"photos"][@"count"] integerValue] > 1 ) {
                NSDictionary *resultDict = result;
                NSArray *fsVenuePhotos = [FSVenuePhoto convertToPhotos:resultDict[@"response"][@"photos"][@"items"] withThumbnailSize:100 withBannerWidth:319 withBannerHeight:159];
                self.imagesArray = [fsVenuePhotos mutableCopy];
                [self prepareImageSourceWithImagesArray:self.imagesArray];
                [self.collectionView reloadData];
                
            }
            else{
                [self setNoImages];
            }
        }
        else
        {
            [self setNoImages];
        }
    }];
    
    
}
- (void)setNoImages
{
    UIImage *noImagesImage = [ImageLibrary ImagesImageWithSize:35 withColor:[UIColor lightGrayColor]];
    UIImageView *noImagesImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, 35, 35)];
    noImagesImageView.center = self.collectionView.center;
    [noImagesImageView setImage:noImagesImage];
    [self.collectionView addSubview:noImagesImageView];
}

-(void)prepareImageSourceWithImagesArray:(NSMutableArray *)array
{
    
    self.imageArrayForSource = [NSMutableArray new];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    for(int i = 0; i<[array count]; i++)
    {
        FSVenuePhoto *venuePhoto = array[i];
        NSString *formattedDate = [dateFormatter stringFromDate:venuePhoto.createdAt];
        FSBasicImage *firstPhoto = [[FSBasicImage alloc]initWithImageURL:venuePhoto.fullsizeURL name:formattedDate];
        [self.imageArrayForSource addObject:firstPhoto];
    }
    self.basicSource = [[FSBasicImageSource alloc]initWithImages:self.imageArrayForSource];
}
-(void)deleteVenueReview:(id)sender
{
    StoredVenueReview *review = self.reviews[self.tableView.indexPathForSelectedRow.row];
    [[DataStore SharedStore] deleteStoredVenueReviewWithParseObject:review.parseObject];
    // [self.tableView deleteRowsAtIndexPaths:@[self.tableView.indexPathForSelectedRow] withRowAnimation:UITableViewRowAnimationFade];
    [self populateReviews];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareRating
{
    //_starRatingImage.starImage = [UIImage imageNamed:@"star.png"];
    _starRatingImage.starImage = [ImageLibrary StarImageWithSize:20 withColor:[UIColor lightGrayColor]];
    
    // _starRatingImage.starHighlightedImage = [UIImage imageNamed:@"starhighlighted.png"];
    _starRatingImage.starHighlightedImage = [ImageLibrary StarImageWithSize:20 withColor:[UIColor redColor]];
    _starRatingImage.maxRating = 5.0;
    _starRatingImage.delegate = self;
    _starRatingImage.horizontalMargin = 1;
    _starRatingImage.editable=NO;
    _starRatingImage.rating= [self.venue calculateRating];
    _starRatingImage.displayMode=EDStarRatingDisplayAccurate;
    _starRatingImage.returnBlock = ^(float rating )
    {
        
        NSLog(@"Reading the starrating rating: %.0f", _starRatingImage.rating);
        NSLog(@"ReturnBlock: Star rating changed to %.1f", rating);
    };
}
- (void)prepareRating:(EDStarRating *)_starRatingImageForCell withRating:(int)venueRating
{
    _starRatingImageForCell.starImage = [ImageLibrary StarImageWithSize:10 withColor:[UIColor lightGrayColor]];
    _starRatingImageForCell.starHighlightedImage = [ImageLibrary StarImageWithSize:10 withColor:[UIColor redColor]];
    _starRatingImageForCell.maxRating = 5.0;
    _starRatingImageForCell.horizontalMargin = 1;
    _starRatingImageForCell.editable=NO;
    _starRatingImageForCell.rating= venueRating;
    _starRatingImageForCell.displayMode=EDStarRatingDisplayAccurate;
}


@end
