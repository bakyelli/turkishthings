//
//  AddVenueViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "VenuesViewController.h"

@interface AddVenueViewController : UITableViewController <UISearchBarDelegate>


@property (strong, nonatomic) NSArray *venues;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) VenuesViewController *vvc;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@end
