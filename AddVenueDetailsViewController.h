//
//  AddVenueDetailsViewController.h
//  TurkishThings
//
//  Created by Basar Akyelli on 11/6/13.
//  Copyright (c) 2013 Basar Akyelli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSVenue.h"
#import "StoredVenue.h"

@interface AddVenueDetailsViewController : UIViewController

@property (strong, nonatomic) FSVenue *venue;
@property (strong, nonatomic) StoredVenue *existingVenue;
@property (assign) BOOL isExistingVenue;

- (IBAction)addButtonPressed:(id)sender;

@end
