Parse.Cloud.afterSave("StoredVenueReview", function(request, response){


var reviewObject = request.object;
var rating = request.object.get("rating");
var storedvenueuniqueid = request.object.get("storedVenueUniqueID");

var storedvenue = Parse.Object.extend("StoredVenue");
var query = new Parse.Query(storedvenue);

query.equalTo("uniqueID", storedvenueuniqueid);

query.find({
	success: function(results)
	{
		var object = results[0];

		var currentRating = object.get("ratingTotal"); 
		var currentRatingCount = object.get("ratingCount");

		reviewObject.set("storedVenue",object);

		reviewObject.save(null, {
			success: function(object){

			},
			error: function(object,error){

			}
		});


		object.set("ratingCount", currentRatingCount+1);
		object.set("ratingTotal", currentRating+rating);

		object.save(null, {
			  success: function(object) {
			   
			  },
			  error: function(object, error) {
			   
			  }
			});
	}


	});
});

Parse.Cloud.beforeDelete("StoredVenueReview", function(request, response){

console.log("Hello");
var rating = request.object.get("rating");
var storedvenueuniqueid = request.object.get("storedVenueUniqueID");

var storedvenue = Parse.Object.extend("StoredVenue");
var query = new Parse.Query(storedvenue);

query.equalTo("uniqueID", storedvenueuniqueid);

query.find({
	success: function(results)
	{
		console.log(results.length);
		var object = results[0];
		console.log(object);
		var currentRating = object.get("ratingTotal"); 
		var currentRatingCount = object.get("ratingCount");
		var finalRating = currentRatingCount-1;
		var finalRatingTotal = currentRating-rating; 

		object.set("ratingCount", finalRating);
		object.set("ratingTotal", finalRatingTotal);

		object.save(null, {
			  success: function(object) {
			   	response.success();

			  },
			  error: function(object, error) {
				response.success();
			  }
			});
	},
	error: function(object, error)
	{
				response.success();
	}

	});
});